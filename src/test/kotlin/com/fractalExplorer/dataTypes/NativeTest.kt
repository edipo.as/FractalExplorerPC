package com.fractalExplorer.dataTypes

import org.junit.jupiter.api.Test

@ExperimentalUnsignedTypes
class NativeTests {
    val tolerancia = 1.0e-6

    val intMax = Int.MAX_VALUE.toUInt()

    @Test
    fun `When complex pow complex, Then return correct values`() {
        val a: Int = -1
        val b: Int = 10

        val c = a + b
        assert(c == 9)

        val au = a.toUInt()
        val bu = b.toUInt()

        val d: UInt = au + bu

        assert(d == 9u)
    }

    @Test
    fun `When compare negative numbers `() {
        assert((-10).toUInt() > Int.MAX_VALUE.toUInt())
    }

    @Test
    fun `When multiply`() {
        val a: Int = 3
        val b: Int = -100

        assert(a * b == -300)
        assert((a.toUInt() * b.toUInt()).toInt() == -300)


    }

    @Test
    fun `When sum Uint, detect overflow `() {
        val a: UInt = UInt.MAX_VALUE - 100u
        val b: UInt = 200u

        val c = a + b
        assert(c == 99u)


        val overflow = c - a > 0u
        assert(overflow)

        val overflow2 = c - b > 0u
        assert(overflow2)
    }

    @Test
    fun `When sum Int, detect overflow `() {
        val a: Int = (UInt.MAX_VALUE - 100u).toInt()
        val b: Int = 200

        val c = a + b
        assert(c == 99)

        println(c - a)
        val overflow = (c - a) > 0
        println("c<a $overflow")
        //assert(overflow)

        println(c - b)
        val overflow2 = (c - b) > 0
        println("c<b $overflow2")
        //assert(overflow2)
    }

}