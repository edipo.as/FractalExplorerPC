@file:Suppress("INTEGER_OVERFLOW")

package com.fractalExplorer.dataTypes

import org.junit.jupiter.api.Test

@ExperimentalUnsignedTypes
internal class ArbNTest {

    @Test
    fun `when sumAssign, overflow`() {
        val a = ArbN(
                0u,
                UInt.MAX_VALUE
        )

        val b = ArbN(
                0u,
                1u
        )

        val c = ArbN(
                1u,
                0u
        )

        a += b

        assert(a == c)
    }

    @Test
    fun `when sumAssign, increase`() {
        val a = ArbN(
                123u,
                456u
        )

        val b = ArbN(
                111u,
                111u
        )

        val c = ArbN(
                234u,
                567u
        )

        a += b

        assert(a == c)
    }

    @Test
    fun `when minusAssign, underflow`() {
        val a = ArbN(
                1u,
                0u
        )

        val b = ArbN(
                0u,
                1u
        )

        val c = ArbN(
                0u,
                UInt.MAX_VALUE
        )

        a -= b

        assert(a == c)
    }

    @Test
    fun `when minusAssign, decrease`() {
        val a = ArbN(
                234u,
                567u
        )

        val b = ArbN(
                111u,
                111u
        )

        val c = ArbN(
                123u,
                456u
        )

        a -= b

        assert(a == c)
    }

    @Test
    fun `when negative number, be same as zero minus positive number`() {
        val a = ArbN(
                0u,
                0u
        )

        val b = ArbN(
                123u,
                254u
        )

        val c = ArbN(
                (-123).toUInt(),
                (-254).toUInt()
        )

        a -= b

        assert(a == c)
    }

    @Test
    fun `when multiply, then do multiply`() {
        val a = ArbN(
                0u,
                0u
        )

        val b = ArbN(
                123u,
                254u
        )

        val c = ArbN(
                (-123).toUInt(),
                (-254).toUInt()
        )

        a -= b

        assert(a == c)
    }
}