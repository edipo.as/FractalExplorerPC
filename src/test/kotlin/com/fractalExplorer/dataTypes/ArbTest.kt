package com.fractalExplorer.dataTypes

import org.junit.jupiter.api.Test

@ExperimentalUnsignedTypes
internal class ArbTest {

    @Test
    fun `when creating from double, then get double value again`() {
        val values = listOf(
                1234.56789,
                -1.45847987,
                UInt.MAX_VALUE.toDouble(),
                Int.MIN_VALUE.toDouble(),
                Int.MAX_VALUE.toDouble()
        )

        values.forEach { value ->
            val arb = Arb(value)
            val returnedValue = arb.toDouble()
            val relativeDifference = (returnedValue - value) / value

            println("value -> $value returned -> $returnedValue diff -> $relativeDifference")
        }


    }
}