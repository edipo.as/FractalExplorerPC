package com.fractalExplorer.dataTypes

import kotlin.math.*

class NumComplexoTest {
    val tolerancia = 1.0e-6

    @org.junit.jupiter.api.Test
    fun `When complex pow complex, Then return correct values`() {
        val inputs = listOf(
                Pair(NumComplexo(0.0, 1.0), NumComplexo(0.0, 1.0))
        )
        val expectedOutputs = listOf(
                NumComplexo(exp(-pi / 2), 0.0)
        )

        inputs.forEachIndexed { i, it ->
            val base = it.first
            val expoente = it.second
            val resultado = base.pow(expoente)
            val residuo = (resultado - expectedOutputs[i])
            val absResiduo = residuo.abs()
            assert(absResiduo < tolerancia)
        }
    }

    @org.junit.jupiter.api.Test
    fun `When complex pow complex, Then return correct value`() {
        val base = NumComplexo(1.0, 1.0)
        val expoente = NumComplexo(1.0, 1.0)

        val k = sqrt(2.0) * exp(-pi / 4.0)
        val theta = pi / 4.0 + ln(2.0) / 2.0

        val resultadoEsperado = NumComplexo(
                k * cos(theta),
                k * sin(theta)
        )

        val resultado = base.pow(expoente)
        val residuo = resultado - resultadoEsperado
        val absResiduo = residuo.abs()

        assert(absResiduo < tolerancia)

    }

    @org.junit.jupiter.api.Test
    fun pow() {
        val inputs = listOf(
                NumComplexo(0.0, 1.0)
        )
        val expectedOutputs = listOf(
                NumComplexo(-1.0, 0.0)
        )

        inputs.forEachIndexed { i, it ->
            val resultado = it.quadrado()
            val residuo = (resultado - expectedOutputs[i])
            val absResiduo = residuo.abs()
            println("abs ${absResiduo.format(3)}")
        }
    }

    @org.junit.jupiter.api.Test
    fun arg() {
        val inputs = listOf(
                NumComplexo(1.0, 1.0),
                NumComplexo(0.0, 1.0),
                NumComplexo(1.0, 0.0)
        )
        val expectedOutputs = listOf(
                pi / 4.0,
                pi / 2.0,
                0.0
        )
        inputs.forEachIndexed { i, it ->
            val resultado = it.arg()
            val residuo = (resultado - expectedOutputs[i])
            println("abs ${resultado.format(3)}")
        }
    }

}