package com.myGraphicLibrary.glfw

import com.fractalExplorer.dataTypes.GLFWWindowHandle
import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFWErrorCallback
import java.lang.Thread.sleep

//todo criar Thread exclusiva openGL

open class GerenciadorDeJanelasGLFW {

    protected val janelas = emptyMap<GLFWWindowHandle, GenericGlfwWindow>().toMutableMap()
    protected val threadsOpenGl = listOf<ThreadGlfwWindow>().toMutableList()

    init {
        // Initialize GLFW. Most GLFW functions will not work before doing this.
        check(GLFW.glfwInit()) { throw RuntimeException("Unable to initialize GLFW") }
        GLFWErrorCallback.createPrint(System.err).set()
    }

    fun addJanela(construtorJanela: () -> GenericGlfwWindow) {
        threadsOpenGl.add(
                ThreadGlfwWindow(construtorJanela).also {
                    it.start()
                    while (!it.finishedStart) {
                        println("WAITING OPENGL")
                        sleep(500)
                    }
                }
        )
    }

    fun startLoop() {
        while (!allWindowsShouldClose()) {
            emCadaQuadro()
            sleep(15)
        }
        aoEncerrar()
        encerrarGLFW()
    }

    open fun emCadaQuadro() {}
    open fun aoEncerrar() {}

    private fun encerrarGLFW() {
        sleep(1000) //sync with glslcalls inside loop thread
        GLFW.glfwSetErrorCallback(null)!!.free()
        // Terminate GLFW and free the error callback
        GLFW.glfwTerminate()

        println("GFLW Terminated")
    }


    private fun allWindowsShouldClose() = threadsOpenGl.all {
        it.glfwWindow.encerrada
    }
}