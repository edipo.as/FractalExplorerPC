package com.myGraphicLibrary.glfw

import com.fractalExplorer.binds.ActionName
import com.fractalExplorer.binds.KeyboardKey
import com.fractalExplorer.dataTypes.CoordenadasTelaI
import com.fractalExplorer.dataTypes.GLFWWindowHandle
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.platformSpecific.windows.Relogio
import com.fractalExplorer.platformSpecific.windows.glfw.GlfwRawConverter
import com.fractalExplorer.platformSpecific.windows.glfw.getDesktopResolution
import com.fractalExplorer.platformSpecific.windows.glfw.getOGLWindowResolution
import com.fractalExplorer.platformSpecific.windows.mouseInput.MouseWrapper
import org.lwjgl.glfw.Callbacks
import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFW.GLFW_SAMPLES
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL32
import org.lwjgl.system.MemoryUtil

// é preciso chamar método exibirJanela
//TODO: encontrar maneira de chamar o método exibirJanela depois do construtor das subclasses
//todo nao permitir que existam janelas sem gerenciador de janelas

open class GenericGlfwWindow(tamanhoInicialTela: CoordenadasTelaI = CoordenadasTelaI(2500, 1500)) {

    fun alterarTitulo(string: String) = GLFW.glfwSetWindowTitle(windowHandle, string)

    protected open fun aoIniciarGlfw() {}
    protected open fun aCadaQuadroGlfw() {}
    var aCadaQuadroGlfwLambda: (() -> Unit)? = null
    protected open fun aoFecharJanela() {}
    protected open fun acaoAoRedimensionar(janela: GLFWWindowHandle, windowSize: Vetor2i) {}

    val keyBinds = emptyMap<KeyboardKey, ActionName>().toMutableMap()
    val actionBinds = emptyMap<ActionName, () -> Unit>().toMutableMap()

    var dimensoesJanela = tamanhoInicialTela
        private set
    private var dimensoesJanelaRestaurada = dimensoesJanela
    private var posicaoJanela = CoordenadasTelaI()

    var mouse = MouseWrapper(listOf(GLFW.GLFW_MOUSE_BUTTON_1, GLFW.GLFW_MOUSE_BUTTON_2, GLFW.GLFW_MOUSE_BUTTON_3))
    val relogio = Relogio()

    var encerrada = false

    var janelaCheia = false
        set(janelaCheia) {
            if (field != janelaCheia) {
                if (janelaCheia) {
                    tornarJanelaCheia()
                } else {
                    restaurarJanela()
                }
            }
            field = janelaCheia
        }

    var janelaVisivel = false
        set(novoValor) {
            if (novoValor) {
                GLFW.glfwShowWindow(windowHandle)
                field = novoValor
            } else {
                TODO("Tornar janela invisível ainda não implementado")
            }
        }

    var windowHandle: GLFWWindowHandle = 0


    init {
        addDefaultBindings()
        addActions()
        println("INIT GenericGlfwWindow")
        GLFWErrorCallback.createPrint(System.err).set()
        check(GLFW.glfwInit()) { throw RuntimeException("Unable to initialize GLFW") }
        inicializarJanelaGLFW()
        println("Instanciou Janela GLFW handle $windowHandle!")
        configuraCalbacksGLFW()

        aoIniciarGlfw()

    }

    private fun addActions() {
        actionBinds[ActionName.WINDOW_FULLSCREEN_TOGGLE] = { janelaCheia = !janelaCheia }
        actionBinds[ActionName.WINDOW_FULLSCREEN_DISABLE] = { janelaCheia = false }
    }

    private fun addDefaultBindings() {
        keyBinds[KeyboardKey.KEY_F] = ActionName.WINDOW_FULLSCREEN_TOGGLE
        keyBinds[KeyboardKey.KEY_ESCAPE] = ActionName.WINDOW_FULLSCREEN_DISABLE
    }

    private fun inicializarJanelaGLFW() {
        val resolucaoDesktop = getDesktopResolution()

        GLFW.glfwDefaultWindowHints() // optional, the current window hints are already the default
        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE) // the window will stay hidden right after creation
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE) // the window will be resizable
        GLFW.glfwWindowHint(GLFW_SAMPLES, 4)
        windowHandle = GLFW.glfwCreateWindow(dimensoesJanela.x, dimensoesJanela.y, "", MemoryUtil.NULL, MemoryUtil.NULL)
                .also {
                    if (it == MemoryUtil.NULL) throw RuntimeException("Failed to create the GLFW window")
                }
        val tamJanela = getOGLWindowResolution(windowHandle)
        posicaoJanela = CoordenadasTelaI((resolucaoDesktop.x - tamJanela.x) / 2, (resolucaoDesktop.y - tamJanela.y) / 2)
        GLFW.glfwSetWindowPos(windowHandle, posicaoJanela.x, posicaoJanela.y)
        GLFW.glfwMakeContextCurrent(windowHandle)
        GLFW.glfwSwapInterval(1)
        GL.createCapabilities()
        GL32.glEnable(GL32.GL_BLEND)
        GL32.glBlendFunc(GL32.GL_SRC_ALPHA, GL32.GL_ONE_MINUS_SRC_ALPHA)
        GL32.glClearColor(0.5f, 0.9f, 0.5f, 0.5f)
        janelaVisivel = true
    }

    private fun desenharQuadroJanela() {
        GLFW.glfwMakeContextCurrent(windowHandle)
        GL32.glClear(GL32.GL_COLOR_BUFFER_BIT or GL32.GL_DEPTH_BUFFER_BIT) // clear the framebuffer
        aCadaQuadroGlfw()
        aCadaQuadroGlfwLambda?.invoke()
        GLFW.glfwSwapBuffers(windowHandle) // swap the color buffers
    }

    private fun encerrar() {
        aoFecharJanela()
        janelaCheia = false
        encerrada = true

        Callbacks.glfwFreeCallbacks(windowHandle)
        GLFW.glfwDestroyWindow(windowHandle)
    }


    private fun restaurarJanela() {
        dimensoesJanelaRestaurada = dimensoesJanela
        val resolucaoMonitorPrimario = getDesktopResolution()
        val monitorPrimario = GLFW.glfwGetPrimaryMonitor()
        GLFW.glfwSetWindowMonitor(windowHandle, monitorPrimario, 0, 0,
                resolucaoMonitorPrimario.x, resolucaoMonitorPrimario.y, 0)
    }

    private fun tornarJanelaCheia() {
        GLFW.glfwSetWindowMonitor(windowHandle, 0, posicaoJanela.x, posicaoJanela.y,
                dimensoesJanelaRestaurada.x, dimensoesJanelaRestaurada.y, 0)
        println(dimensoesJanela)
    }

    protected open fun acaoTeclas(key: KeyboardKey, action: Int, mods: Int) {
        if (key == KeyboardKey.KEY_ESCAPE && action == GLFW.GLFW_RELEASE) {
            GLFW.glfwSetWindowShouldClose(windowHandle, true)
        }
        if (action in listOf(GLFW.GLFW_PRESS, GLFW.GLFW_REPEAT)) {
            val actionName = keyBinds[key]
            println("KEY $key")
            println("ACTION $actionName")
            actionBinds[actionName]?.invoke()
        }
    }

    private fun configuraCalbacksGLFW() {
        GLFW.glfwSetMouseButtonCallback(windowHandle) { window: Long, button: Int, action: Int, mods: Int ->
            if (window == windowHandle) {
                mouse.handleMouseButton(button, action)
            }
        }
        GLFW.glfwSetCursorPosCallback(windowHandle) { window: Long, xpos: Double, ypos: Double ->
            if (window == windowHandle) {
                mouse.handleMouseMovement(xpos.toInt(), ypos.toInt())
            }
        }
        GLFW.glfwSetScrollCallback(windowHandle) { window, xoffset, yoffset ->
            if (window == windowHandle) {
                mouse.handleScrollAction(yoffset)
            }
        }
        GLFW.glfwSetWindowSizeCallback(windowHandle) { window: Long, x: Int, y: Int ->
            if (window == windowHandle) {
                dimensoesJanela = CoordenadasTelaI(x, y)
                acaoAoRedimensionar(window, Vetor2i(x, y))
            }
        }
        GLFW.glfwSetKeyCallback(windowHandle) { window: Long, key: Int, scancode: Int, action: Int, mods: Int ->
            if (window == windowHandle) {
                acaoTeclas(GlfwRawConverter.convertRawKey(key), action, mods)
            }
        }
    }

    //todo remover essa logica, deixar tudo na thread opengl
    fun loop() {
        if (GLFW.glfwWindowShouldClose(windowHandle)) {
            encerrar()
        } else {
            desenharQuadroJanela()
            GLFW.glfwPollEvents()
        }

    }


}