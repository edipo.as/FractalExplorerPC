package com.myGraphicLibrary.glfw

import com.fractalExplorer.threads.LoopThread

class ThreadGlfwWindow(private val windowConstructor: () -> GenericGlfwWindow) : LoopThread() {

    override val threadName = "OpenGl"

    var finishedStart = false

    lateinit var glfwWindow: GenericGlfwWindow

    override fun loop() {
        if (!glfwWindow.encerrada) {
            glfwWindow.loop()
        } else {
            interrupt()
        }
    }

    override fun aoEncerrar() {

    }

    override fun aoIniciar() {
        glfwWindow = windowConstructor.invoke()
        finishedStart = true
    }
}