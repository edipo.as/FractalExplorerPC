package com.fractalExplorer.interfaces

interface Relogio {
    fun getCurrentTimeMs(): Long
}