package com.fractalExplorer.interfaces

import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.openGl.TextureWrapper

interface GerenciadorDeImplementacoesInterface {
    fun relogio(): Relogio
    fun texturaCelula(textureSize: Vetor2i, pixels: TArrayPixels32): TextureWrapper
    fun texturaPaleta(textureSize: Vetor2i, pixels: TArrayPixels32): TextureWrapper
}