package com.fractalExplorer.dataTypes

class Vetor2i(
        val x: TipoBaseVetor2i = 0,
        val y: TipoBaseVetor2i = 0
) {

    operator fun unaryMinus() = Vetor2i(-x, -y)
    operator fun plus(V: Vetor2i) = Vetor2i(x + V.x, y + V.y)
    operator fun minus(V: Vetor2i) = Vetor2i(x - V.x, y - V.y)
    operator fun times(V: Vetor2i) = Vetor2i(x * V.x, y * V.y)
    operator fun times(k: TipoBaseVetor2i) = Vetor2i(x * k, y * k)
    operator fun div(k: TipoBaseVetor2i) = Vetor2i(x / k, y / k)

    fun coerceIn(min: Vetor2i, max: Vetor2i) = Vetor2i(
            x.coerceIn(min.x, max.x),
            y.coerceIn(min.y, max.y)
    )

    fun area() = x * y

    override fun toString(): String {
        return "x: $x y: $y"
    }
}
