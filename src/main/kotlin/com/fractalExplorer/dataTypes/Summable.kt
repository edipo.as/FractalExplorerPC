package com.fractalExplorer.dataTypes

interface Summable<T>{

    operator fun unaryMinus(): T
    operator fun plus(V: T): T
    operator fun minus(V: T): T
    operator fun times(k: Double): T

    fun coerceIn(min: T, max: T): T

}