package com.fractalExplorer.dataTypes

//todo descobrir como isso funciona

fun Vetor2i.toVetor2d() = Vetor2d(x.toDouble(), y.toDouble())

fun CoordenadasTelaI.toCoordenadasTelaF() = CoordenadasTelaF(x.toDouble(), y.toDouble())

fun CoordenadasTelaF.toCoordenadasTelaI() = CoordenadasTelaI(x.toInt(), y.toInt())

fun CoordenadasTelaF.toCoordenadasPlano(camera: CameraPosition) = CoordenadasPlano(
        camera.pos.x + x * camera.delta * 2.0,
        camera.pos.y + y * camera.delta * 2.0
)

fun CoordenadasPlano.toCoordenadasTela(camera: CameraPosition) = CoordenadasTelaF(
        (x - camera.pos.x) / (camera.delta),
        (y - camera.pos.y) / (camera.delta)
)

fun Double.format(digits: Int) = "%.${digits}f".format(this)

fun AngDegrees.toGraus() = this * 180 / pi

fun Boolean.toCppStyleInt(): Int = if (this) {
    1
} else {
    0
}

