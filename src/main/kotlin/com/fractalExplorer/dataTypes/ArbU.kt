package com.fractalExplorer.dataTypes

import kotlin.math.max

//todo  z=square(n)
//todo  z*=n
//todo  z+=n
//todo  z=a-b

typealias NumArray = IntArray
typealias Num = Int

const val halfBits = 16

abstract class ArbitraryPrecisionNumber : Number()

@ExperimentalUnsignedTypes
data class ArbN(val precision: Int = 1) : ArbitraryPrecisionNumber() {

    private var array = NumArray(precision + 1)

    //todo does data class auto implement this?
    override fun equals(other: Any?): Boolean {
        //  if (other == null) { return  false}
        if (other is ArbN) {
            val maxprecision = max(precision, other.precision)
            for (index in maxprecision downTo 1) {
                if (array[index] != other.array[index]) return false
            }
            return true
        } else {
            return false
        }
    }

    operator fun plusAssign(N: ArbN) {
        val maxprecision = max(precision, N.precision)
        for (index in maxprecision downTo 1) {
            array[index] += N.array[index]
            if (array[index] < N.array[index]) (array[index - 1]++)
        }
        array[0] += N.array[0]
    }

    operator fun minusAssign(N: ArbN) {
        val maxprecision = max(precision, N.precision)
        for (index in maxprecision downTo 1) {
            array[index] -= N.array[index]
            if (array[index] > N.array[index]) (array[index - 1]--)
        }
        array[0] -= N.array[0]
    }

    operator fun timesAssign(N: ArbN) {

        val maxprecision = max(precision, N.precision)
        if (maxprecision != 1) {
            TODO("Not implemented")
        }
        for (index in maxprecision downTo 1) {
            val msbA = array
            array[index] -= N.array[index]
            if (array[index] > N.array[index]) (array[index - 1]--)
        }
        array[0] -= N.array[0]
    }

    constructor(v0: UInt, v1: UInt, string: String = "") : this(1) {
        array[0] = v0.toInt()
        array[1] = v1.toInt()

    }

    fun imul(a: Num, b: Num): Pair<Num, Num> {
        val lsbA = a.shl(halfBits)
        val lsbB = b.shl(halfBits)

        TODO("")


    }


    /*
    operator fun unaryMinus() = Arb(-v)
    operator fun unaryPlus() = this
    fun coerceIn(min: Arb, max: Arb) = Arb(v.coerceIn(min.v, max.v))

    operator fun plus(V: Arb) = Arb(v + V.v)
    operator fun minus(V: Arb) = Arb(v - V.v)
    operator fun times(V: Arb) = Arb(v * V.v)
    operator fun div(V: Arb) = Arb(v / V.v)
    operator fun compareTo(V: Arb) = v.compareTo(V.v)
    fun pow(V: Arb): Arb = Arb(v.pow(V.v))

    operator fun plus(V: Double) = Arb(v + V)
    operator fun minus(V: Double) = Arb(v - V)
    operator fun times(V: Double) = Arb(v * V)
    operator fun div(V: Double) = Arb(v / V)
    operator fun compareTo(V: Double) = v.compareTo(V)
    fun pow(V: Double): Arb = Arb(v.pow(V))

    operator fun plus(V: Int) = Arb(v + V)
    operator fun minus(V: Int) = Arb(v - V)
    operator fun times(V: Int) = Arb(v * V)
    operator fun div(V: Int) = Arb(v / V)
    operator fun compareTo(V: Int) = v.compareTo(V)

    fun pow(V: Int): Arb = Arb(v.pow(V))
    fun ln(): Arb = Arb(kotlin.math.ln(v))
    fun exp() = Arb(kotlin.math.exp(v))
    fun log(base: Arb) = Arb(kotlin.math.log(v, base.v))
    fun log(base: Double) = Arb(kotlin.math.log(v, base))*/

    override fun toByte(): Byte {
        TODO("Not yet implemented")
    }

    override fun toChar(): Char {
        TODO("Not yet implemented")
    }

    override fun toDouble(): Double {
        TODO("Not yet implemented")
    }

    override fun toFloat(): Float {
        TODO("Not yet implemented")
    }

    override fun toInt(): Int {
        TODO("Not yet implemented")
    }

    override fun toLong(): Long {
        TODO("Not yet implemented")
    }

    override fun toShort(): Short {
        TODO("Not yet implemented")
    }

    override fun toString(): String {
        var string = ""
        array.forEach {
            string += " ${it.toUInt().toString(radix = 2)} "
        }
        return string
    }

/*
    fun toStringFormat(digits: Int): String {
        return v.format(digits)
    }*/

    /*
    operator fun Double.plus(V: Arb) = V.plus(this)
    operator fun Double.minus(V: Arb) = V.minus(this)
    operator fun Double.times(V: Arb) = V.times(this)
    operator fun Double.div(V: Arb) = V.div(this)

    operator fun Int.plus(V: Arb) = V.plus(this)
    operator fun Int.minus(V: Arb) = V.minus(this)
    operator fun Int.times(V: Arb) = V.times(this)
    operator fun Int.div(V: Arb) = V.div(this)*/

}


