package com.fractalExplorer.dataTypes

typealias CoordenadaPlano = Double
typealias CoordenadasPlano = Vetor2d    //** Z = x + y*j **/
//typealias CoordenadaTela = Double       //** pixel  **/
typealias CoordenadasTelaF = Vetor2d     //** pixels **/
//typealias CoordenadaTelaI = Int         //** pixel  **/
typealias CoordenadasTelaI = Vetor2i    //** pixels **/
typealias TDelta = Double
typealias AngRads = Double
typealias AngDegrees = Double
typealias TCor = CorRGB
typealias TIteracoes = Int
typealias TArrayIteracoes = IntArray
typealias TArrayPixels32 = IntArray
typealias FatorAmpliacaoI = Int
typealias IdxPaleta = Int
typealias CorRGBA8 = Int
typealias TimeSecondsI = Long
typealias FrequencyRads = Double
typealias TimeMilliseconds = Double
typealias TimeMillisecondsI = Long

/** tipos base das classes especiais */
typealias TipoBaseNumComplexo = Double
typealias TipoBaseVetor2i = Int
typealias TipoBaseVetor2d = Double

/** tipos do GLFW **/
typealias InteiroGFLW = Int
typealias IBotaoMouse = InteiroGFLW
typealias IAcaoBotao = InteiroGFLW
typealias TeclaGLFW = InteiroGFLW
typealias GLFWWindowHandle = Long

/** tipos do OpenGL **/
typealias InteiroOGL = Int
typealias UniformHandle = Int
typealias ProgramHandle = Int
typealias ITexturaGL = Int

const val pi = 3.14159265358979311599796346854