package com.fractalExplorer.dataTypes

import java.lang.Double.NaN
import kotlin.math.*

class NumComplexo(
        val real: TipoBaseNumComplexo = 0.0,
        val imag: TipoBaseNumComplexo = 0.0
) {
    constructor(C: NumComplexo) : this(C.real, C.imag)

    fun abs() = sqrt(imag * imag + real * real)

    //todo revisar quadrantes
    fun arg(): TipoBaseNumComplexo {
        return if (imag != 0.0) {
            2 * atan((abs() - real) / imag)
        } else {
            if (real == 0.0) {
                NaN
            } else {
                if (real > 0.0) {
                    pi
                } else {
                    0.0
                }
            }
        }
    }


    operator fun plus(k: TipoBaseNumComplexo) = NumComplexo(real + k, imag)
    operator fun minus(k: TipoBaseNumComplexo) = NumComplexo(real - k, imag)
    operator fun times(k: TipoBaseNumComplexo) = NumComplexo(real * k, imag * k)
    operator fun div(k: TipoBaseNumComplexo) = NumComplexo(real / k, imag / k)

    operator fun plus(C: NumComplexo) = NumComplexo(real + C.real, imag + C.imag)
    operator fun minus(C: NumComplexo) = NumComplexo(real - C.real, imag - C.imag)
    operator fun times(C: NumComplexo) = NumComplexo(
            real * C.real - imag * C.imag,
            imag * C.real + real * C.imag
    )

    operator fun div(C: NumComplexo) = NumComplexo(
            real * C.real + imag * C.imag,
            imag * C.real - real * C.imag
    ) / (C.real * C.real + C.imag * C.imag)

    fun quadrado() = NumComplexo(
            real * real - imag * imag,
            2 * real * imag
    )

    // todo otimizar
    fun cubo() = times(this).times(this)
    fun pow(expoente: Int): NumComplexo {
        var number = this
        repeat(expoente - 1) {
            number = number.times(this)
        }
        return number
    }

    fun pow(expoente: NumComplexo): NumComplexo {
        val abs2 = (real * real + imag * imag)
        val arg = arg()
        val k = abs2.pow(expoente.real / 2.0) * exp(-expoente.imag * arg)
        val theta = expoente.real * arg + expoente.imag / 2.0 * ln(abs2)
        return NumComplexo(
                k * cos(theta),
                k * sin(theta)
        )
    }

    override fun toString() = "${real.format(3)} + i*${imag.format(3)}"
}
