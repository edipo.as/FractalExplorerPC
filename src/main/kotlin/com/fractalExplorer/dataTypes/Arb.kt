package com.fractalExplorer.dataTypes

import kotlin.math.pow

class Arb(private val v: Double = 0.0) : Number() {

    operator fun unaryMinus() = Arb(-v)
    operator fun unaryPlus() = this
    fun coerceIn(min: Arb, max: Arb) = Arb(v.coerceIn(min.v, max.v))

    operator fun plus(V: Arb) = Arb(v + V.v)
    operator fun minus(V: Arb) = Arb(v - V.v)
    operator fun times(V: Arb) = Arb(v * V.v)
    operator fun div(V: Arb) = Arb(v / V.v)
    operator fun compareTo(V: Arb) = v.compareTo(V.v)
    fun pow(V: Arb): Arb = Arb(v.pow(V.v))

    operator fun plus(V: Double) = Arb(v + V)
    operator fun minus(V: Double) = Arb(v - V)
    operator fun times(V: Double) = Arb(v * V)
    operator fun div(V: Double) = Arb(v / V)
    operator fun compareTo(V: Double) = v.compareTo(V)
    fun pow(V: Double): Arb = Arb(v.pow(V))

    operator fun plus(V: Int) = Arb(v + V)
    operator fun minus(V: Int) = Arb(v - V)
    operator fun times(V: Int) = Arb(v * V)
    operator fun div(V: Int) = Arb(v / V)
    operator fun compareTo(V: Int) = v.compareTo(V)

    fun pow(V: Int): Arb = Arb(v.pow(V))
    fun ln(): Arb = Arb(kotlin.math.ln(v))
    fun exp() = Arb(kotlin.math.exp(v))
    fun log(base: Arb) = Arb(kotlin.math.log(v, base.v))
    fun log(base: Double) = Arb(kotlin.math.log(v, base))

    override fun toByte(): Byte {
        TODO("Not yet implemented")
    }

    override fun toChar(): Char {
        TODO("Not yet implemented")
    }

    override fun toDouble(): Double {
        return v.toDouble()
    }

    override fun toFloat(): Float {
        return v.toFloat()
    }

    override fun toInt(): Int {
        return v.toInt()
    }

    override fun toLong(): Long {
        return v.toLong()
    }

    override fun toShort(): Short {
        TODO("Not yet implemented")
    }

    override fun toString(): String {
        return "$v"
    }

    fun toStringFormat(digits: Int): String {
        return v.format(digits)
    }

    operator fun Double.plus(V: Arb) = V.plus(this)
    operator fun Double.minus(V: Arb) = V.minus(this)
    operator fun Double.times(V: Arb) = V.times(this)
    operator fun Double.div(V: Arb) = V.div(this)

    operator fun Int.plus(V: Arb) = V.plus(this)
    operator fun Int.minus(V: Arb) = V.minus(this)
    operator fun Int.times(V: Arb) = V.times(this)
    operator fun Int.div(V: Arb) = V.div(this)
}


