package com.fractalExplorer.dataTypes

data class CameraPosition(
    val pos: CoordenadasPlano = CoordenadasPlano(),
    val delta: TDelta = 0.0,
    val angulo: Angle = Angle()
) : Summable<CameraPosition> {

    override fun coerceIn(min: CameraPosition, max: CameraPosition) = CameraPosition(
        pos.coerceIn(min.pos, max.pos),
        delta.coerceIn(min.delta, max.delta),
        angulo.coerceIn(min.angulo, max.angulo)
    )

    override fun unaryMinus() = CameraPosition(
        -pos,
        -delta,
        -angulo
    )

    override operator fun plus(V: CameraPosition) = CameraPosition(
        pos + V.pos,
        delta + V.delta,
        angulo + V.angulo
    )

    override operator fun minus(V: CameraPosition) = CameraPosition(
        pos - V.pos,
        delta - V.delta,
        angulo - V.angulo
    )

    override operator fun times(k: Double)= CameraPosition (
        pos*k,
        delta*k,
        angulo *k
    )
}