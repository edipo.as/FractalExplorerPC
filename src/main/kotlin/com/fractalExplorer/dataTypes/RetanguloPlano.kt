package com.fractalExplorer.dataTypes

class RetanguloPlano {
    var min = CoordenadasPlano()
    var max = CoordenadasPlano()

    constructor()

    constructor(min: CoordenadasPlano, max: CoordenadasPlano) {
        this.min = min
        this.max = max
    }

    constructor(C: RetanguloPlano) {
        min = C.min
        max = C.max
    }

    override fun toString() = "min = $min, max = $max"
}