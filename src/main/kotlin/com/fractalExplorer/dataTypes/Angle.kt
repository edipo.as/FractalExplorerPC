package com.fractalExplorer.dataTypes

import kotlin.math.cos
import kotlin.math.sin

class Angle : Summable<Angle>{
    private var degs: AngDegrees = 0.0

        private set(value) {
            field = value

            val rads = radians(value)

            val sin = sin(rads)
            val cos = cos(rads)
            matrizRot[0][0] = cos
            matrizRot[0][1] = sin
            matrizRot[1][0] = -sin
            matrizRot[1][1] = cos
        }

    fun cos() = matrizRot[0][0].toFloat()
    fun sen() = matrizRot[0][1].toFloat()

    private var matrizRot = Array(2) { DoubleArray(2) { 0.0 } }

    constructor() : super()

    constructor(c: Angle) {
        degs = c.degs
    }

    constructor(angulo: AngDegrees) {
        this.degs = angulo
    }

    operator fun invoke() = degs

    operator fun plusAssign(b: AngDegrees) {
        degs += b
    }

    override fun coerceIn(min: Angle, max: Angle) = Angle(degs.coerceIn(min.degs, max.degs))
    override operator fun unaryMinus() = Angle(-degs)
    override operator fun plus(V: Angle) = Angle(degs + V.degs)
    override operator fun minus(V: Angle) = Angle(degs - V.degs)
    override operator fun times(k: Double) = Angle(degs * k)

    operator fun minusAssign(b: AngDegrees) {
        degs -= b
    }

    operator fun times(v: Vetor2d) = Vetor2d(
        matrizRot[0][0] * v.x + matrizRot[0][1] * v.y,
            matrizRot[1][0] * v.x + matrizRot[1][1] * v.y
    )

    private fun radians(degrees: Double) = degrees * pi/  180.0
    private fun degrees(degrees: Double) = degrees * 180.0/ pi
}
