package com.fractalExplorer.dataTypes

class CorRGB {
    var r: Int = 0
    var g: Int = 0
    var b: Int = 0

    constructor(corRGBA8: CorRGBA8) {
        r = corRGBA8 % 256
        b = corRGBA8.shr(8) % 256
        g = corRGBA8.shr(16) % 256
    }

    constructor(R: Int, G: Int, B: Int) {
        r = R.coerceIn(0, 255)
        g = G.coerceIn(0, 255)
        b = B.coerceIn(0, 255)
    }

    constructor(R: Float, G: Float, B: Float) {
        r = (R * 255f).toInt().coerceIn(0, 255)
        g = (G * 255f).toInt().coerceIn(0, 255)
        b = (B * 255f).toInt().coerceIn(0, 255)
    }

    constructor(R: Double, G: Double, B: Double) {
        r = (R * 255f).toInt().coerceIn(0, 255)
        g = (G * 255f).toInt().coerceIn(0, 255)
        b = (B * 255f).toInt().coerceIn(0, 255)
    }

    fun toInt() = 255.shl(24) + b.shl(16) + g.shl(8) + r

    override fun toString() = "R: $r G: $g B: $b"

    companion object {
        fun fromBRG(cor: Int) =
                CorRGB(
                        cor.shr(16) % 256,
                        cor.shr(8) % 256,
                        cor % 256
                )
    }
}
