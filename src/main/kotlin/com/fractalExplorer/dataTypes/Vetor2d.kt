package com.fractalExplorer.dataTypes

import kotlin.math.atan
import kotlin.math.sqrt

open class Vetor2d(
    val x: TipoBaseVetor2d = 0.0,
    val y: TipoBaseVetor2d = 0.0
) : Summable<Vetor2d> {

    override operator fun unaryMinus() = Vetor2d(-x, -y)
    override operator fun plus(V: Vetor2d) = Vetor2d(x + V.x, y + V.y)
    override operator fun minus(V: Vetor2d) = Vetor2d(x - V.x, y - V.y)
    operator fun times(V: Vetor2d) = Vetor2d(x * V.x, y * V.y)
    override operator fun times(k: TipoBaseVetor2d) = Vetor2d(x * k, y * k)
    operator fun div(k: TipoBaseVetor2d) = Vetor2d(x / k, y / k)

    override fun coerceIn(min: Vetor2d, max: Vetor2d) = Vetor2d(
        x.coerceIn(min.x, max.x),
        y.coerceIn(min.y, max.y)
    )

    override fun toString(): String {
        return "x: ${x.format(2)} y: ${y.format(2)}"
    }

    fun toStringFormat(digits: Int): String {
        return "x: ${x.format(digits)} y: ${y.format(digits)}"
    }

    //todo unificar com complexo
    fun abs() = sqrt(x * x + y * y)

    //todo revisar quadrantes
    fun arg(): TipoBaseNumComplexo {
        return if (y != 0.0) {
            2 * atan((abs() - x) / y)
        } else {
            if (x == 0.0) {
                // todo Double.NaN
                0.0
            } else {
                if (x > 0.0) {
                    pi
                } else {
                    0.0
                }
            }
        }
    }


}