package com.fractalExplorer

import com.fractalExplorer.dataTypes.TimeSecondsI
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

//todo interromper execução dos pools ao fechar o programa

//todo destrutor de objetos???

class ObjectPool<T>(
    private val minIdle: Int, private var maxIdle: Int, validationInterval: TimeSecondsI,
    val construtor: () -> T
) {

    private val poolRoutine = {
        val size: Int = pool.size
        if (size < minIdle) {
            val sizeToBeAdded = minIdle - size
            for (i in 0 until sizeToBeAdded) {
                pool.add(construtor())
            }
        } else if (size > maxIdle) {
            val sizeToBeRemoved = size - maxIdle
            for (i in 0 until sizeToBeRemoved) {
                pool.poll()
            }
        }
    //    println("POOLING ${pool.size}")
    }

    private var executorService: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor().apply {
        scheduleWithFixedDelay(poolRoutine, validationInterval, validationInterval, TimeUnit.SECONDS)
    }

    var pool: ConcurrentLinkedQueue<T> = ConcurrentLinkedQueue()


    init {
        maxIdle = maxOf(maxIdle, minIdle)
        initialize(minIdle)

        // check pool conditions in a separate thread
        executorService
    }

    fun borrowObject(): T {
        return pool.poll() ?: construtor()
    }


    fun returnObject(objeto: T?) {
        objeto?.let { pool.offer(it) }
    }

    fun shutdown() {
        executorService.shutdown()
        println("SHUTDOWN")
    }

    private fun initialize(minIdle: Int) {
        pool = ConcurrentLinkedQueue<T>()
        for (i in 0 until minIdle) {
            pool.add(construtor())
        }
    }
}