package com.fractalExplorer.openGl

import com.fractalExplorer.dataTypes.Angle
import com.fractalExplorer.dataTypes.CoordenadasTelaF
import com.fractalExplorer.dataTypes.Vetor2d
import com.fractalExplorer.dataTypes.Vetor2i

interface GlProgramSprite {
    fun desenharSpriteQuadrado(sprite: Sprite, tamSprite: Vetor2d)
}

interface glProgramSpriteArrayOpenGL {
    fun desenharSpriteArray(celula: Sprite, tamSprite: Vetor2d, angle: Angle, indice: Vetor2i, coordenadasCantoSupEsq: CoordenadasTelaF)
}
