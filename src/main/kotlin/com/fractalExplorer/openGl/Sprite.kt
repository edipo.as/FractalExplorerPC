package com.fractalExplorer.openGl

import com.fractalExplorer.dataTypes.Angle
import com.fractalExplorer.dataTypes.Vetor2d
import com.fractalExplorer.threads.runnables.ListaDeTarefas
import com.fractalExplorer.threads.runnables.TarefaCriarTexturaGL
import java.util.concurrent.locks.ReentrantLock

open class Sprite(texturaInicial: TextureWrapper) {
    //Coordenadas do canto superior esquerdo, em pixels, com origem no centro da tela

    var invisible =  false
    var posRelativeToParent = Vetor2d()
    var posAbsoluteToParent = Vetor2d()

    var sizeRelativeToParent = Vetor2d()
    var sizeAbsolute = Vetor2d()

    var rotationRelativeToParent = Angle()

    val childs = emptyList<Sprite>().toMutableList()

    var rotation: Angle = Angle()
    var posAbsolute: Vetor2d = Vetor2d()
    var posRelative: Vetor2d = Vetor2d()
    var sizeRelative: Vetor2d = Vetor2d()

    fun fixchildrenPositions(){
        childs.forEach { child->
            child.rotation = rotation + child.rotationRelativeToParent
            child.posAbsolute = posAbsolute + child.posAbsoluteToParent
            child.posRelative = posRelative + child.posRelativeToParent*sizeRelative
            child.sizeRelative = sizeRelative * child.sizeRelativeToParent
            child.fixchildrenPositions()
        }
    }

    var parent: Sprite? = null

    var textura: TextureWrapper = texturaInicial
        set(novaTextura) {
            lock.lock()
            field = novaTextura
            textura.texturaCorrespondeAMatrizPixels = false
            lock.unlock()
        }

    private val lock = ReentrantLock()

    var marcadaParaDestruicao = false

    fun bindTexture() {
        lock.lock()
        textura.bind()
        lock.unlock()
    }

    // todo passar para setter da classe texturewrapper

    fun possuiTexturaValida(): Boolean {
        return textura.texturaCorrespondeAMatrizPixels
    }

    //todo injecao de dependencias feia em

    /** usado caso o contexto opencl seja resetado*/
    protected fun solicitarGeracaoDeTexturaGL(listaDeTarefas: ListaDeTarefas<TarefaCriarTexturaGL>) {
        listaDeTarefas.add(TarefaCriarTexturaGL(textura))
    }

}

//todo liberar recursos