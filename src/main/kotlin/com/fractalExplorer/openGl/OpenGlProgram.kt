package com.fractalExplorer.openGl

interface OpenGlProgram {

    var programHandle: Int

    fun useProgram() {
    }
}