package com.fractalExplorer.openGl

import com.fractalExplorer.dataTypes.ITexturaGL
import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i

abstract class TextureWrapper(val textureSize: Vetor2i) {
    abstract fun bind()
    abstract fun createOGLTexture()
    abstract fun liberarRecursosOGL()

    //todo criar classe para atrelar os dois???
    abstract val texturaGlEnum: ITexturaGL
    abstract val idxUniformTextura: Int

    var texturaCorrespondeAMatrizPixels = false
    var possuiTexturaOGLAlocada = false

    var iteracoes: TArrayPixels32? = null

    var textureHandle = IntArray(1) { 0 }

    constructor(textureSize: Vetor2i, iteracoes: TArrayPixels32) : this(textureSize) {
        this.iteracoes = iteracoes
    }

    fun possuiTexturaOGLAlocada() = possuiTexturaOGLAlocada

    final override fun toString(): String {
        return "Textura possui Handle GL: " + textureHandle[0].toString() //" + " Buffer Handle: " + bufferHandle[0].toString()
    }
}
