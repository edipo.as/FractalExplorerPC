package com.fractalExplorer.fractal

import com.fractalExplorer.dataTypes.NumComplexo
import com.fractalExplorer.dataTypes.TIteracoes
import com.fractalExplorer.openGl.pixelNaoDivergiu

class MandelbrotPotenciaArbitraria(private val expoente: Int) : Mandelbrot() {

    /** retorna inteiro onde I = iteracoesNormalizadaI/sampling **/
    /** sampling representa o numero de valores intermediario entre um inteiro e outro */
    override fun iteracoesNormalizadaI(
            Cx: Double,
            Cy: Double,
            limiteDivergencia: Int,
            maxIteracoes: Int,
            sampling: Int
    ): TIteracoes {

        val c = NumComplexo(Cx, Cy)
        var z = NumComplexo(c)

        var i = 0
        while (z.abs() < limiteDivergencia) {
            i++
            z = z.pow(expoente) + c
            if (i == maxIteracoes) return pixelNaoDivergiu
        }
        return (i * sampling)
    }
}

