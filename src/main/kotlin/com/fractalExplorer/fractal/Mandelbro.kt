package com.fractalExplorer.fractal

import com.fractalExplorer.dataTypes.CoordenadasPlano

abstract class Mandelbrot : Fractal {
    var coordenadasJulia: CoordenadasPlano? = null
}