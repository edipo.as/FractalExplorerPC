package com.fractalExplorer.fractal

import com.fractalExplorer.dataTypes.NumComplexo
import com.fractalExplorer.dataTypes.TIteracoes
import com.fractalExplorer.openGl.pixelNaoDivergiu
import kotlin.math.sqrt

//todo shader especial e parametros de entrada para fractal de newton
//todo função e derivada como parametro
//todo raiz de funcao
class FractalDeNewton : Fractal {
    val limite = 2.0

    private fun funcaoZ(z: NumComplexo) = z.pow(3) - 1.0
    private fun derivadaZ(z: NumComplexo) = z.quadrado() * 3.0

    private val raizes = listOf(
            NumComplexo(+1.0, 0.0),
            NumComplexo(-0.5, -sqrt(3.0) / 2.0),
            NumComplexo(+0.5, +sqrt(3.0) / 2.0)
    )
    private val cores = listOf(
        1000, 2000, 3000
    )
    /** retorna inteiro onde I = iteracoesNormalizadaI/sampling **/
    /** sampling representa o numero de valores intermediario entre um inteiro e outro */
    override fun iteracoesNormalizadaI(
        Cx: Double,
        Cy: Double,
        limiteDivergencia: Int,
        maxIteracoes: Int,
        sampling: Int
    ): TIteracoes {

        var z = NumComplexo(Cx, Cy)

        var i = 0
        while (true) {
            i++

            z -= funcaoZ(z) / derivadaZ(z)

            val tolerancia = 1.0e-6

            repeat(raizes.size) { idx ->
                val diferenca = z - raizes[idx]
                if (diferenca.abs() < tolerancia) {
                    return i + cores[idx]// i*sampling.toLong()//
                }
            }
            if (i == 100) return pixelNaoDivergiu
        }
    }
}
/*
fun funcaoZ(z:NumComplexo) = z.pow(3)*2.0+1.0
fun derivadaZ(z:NumComplexo) = z.quadrado()*3.0
val numerador = z.pow(3)*2.0+1.0
val denominador = z.quadrado()*3.0
*/