package com.fractalExplorer.fractal

import com.fractalExplorer.dataTypes.TIteracoes
import com.fractalExplorer.openGl.pixelNaoDivergiu
import kotlin.math.ln

class MandelbrotOtimizado : Mandelbrot() {

    /** retorna inteiro onde I = iteracoesNormalizadaI/sampling **/
    /** sampling representa o numero de valores intermediario entre um inteiro e outro */
    override fun iteracoesNormalizadaI(
            Cx: Double,
            Cy: Double,
            limiteDivergencia: Int,
            maxIteracoes: Int,
            sampling: Int
    ): TIteracoes {
        var i = 0
        var zx = Cx
        var zy = Cy
        var zx2 = zx * zx
        var zy2 = zy * zy
        while (zx2 + zy2 < limiteDivergencia) {
            i++
            zy *= zx
            zy += zy
            zy += Cy
            zx = zx2 - zy2 + Cx
            zx2 = zx * zx
            zy2 = zy * zy
            if (i == maxIteracoes) return pixelNaoDivergiu
        }
        i += 10 //garante i > 0 para todos C na aura
        val samplingIteracoes = sampling.toDouble()
        val logZn = ln(zx2 + zy2) / 2.0
        val parcial =
                samplingIteracoes * ln(logZn / 0.69314718056) / 0.69314718056 //parcela que falta para chegar no proximo i
        return (i * samplingIteracoes - parcial).toInt()
    }
}

