package com.fractalExplorer.fractal

import com.fractalExplorer.dataTypes.TIteracoes

interface Fractal {
    fun iteracoesNormalizadaI(
        Cx: Double,
        Cy: Double,
        limiteDivergencia: Int,
        maxIteracoes: Int,
        sampling: Int
    ): TIteracoes
}
