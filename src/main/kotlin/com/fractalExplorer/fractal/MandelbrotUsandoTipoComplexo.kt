package com.fractalExplorer.fractal

import com.fractalExplorer.dataTypes.NumComplexo
import com.fractalExplorer.dataTypes.TIteracoes
import com.fractalExplorer.openGl.pixelNaoDivergiu
import kotlin.math.ln

class MandelbrotUsandoTipoComplexo : Mandelbrot() {

    /** retorna inteiro onde I = iteracoesNormalizadaI/sampling **/
    /** sampling representa o numero de valores intermediario entre um inteiro e outro */
    override fun iteracoesNormalizadaI(
            Cx: Double,
            Cy: Double,
            limiteDivergencia: Int,
            maxIteracoes: Int,
            sampling: Int
    ): TIteracoes {

        val c = NumComplexo(Cx, Cy)
        var z = NumComplexo(c)

        var i = 0
        while (z.abs() < limiteDivergencia) {
            i++
            z = z.quadrado() + c
            if (i == maxIteracoes) return pixelNaoDivergiu
        }
        i += 10 //garante i > 0 para todos C na aura
        val samplingIteracoes = sampling.toDouble()
        val logZn = ln(z.real * z.real + z.imag * z.imag) / 2.0
        val parcial =
                samplingIteracoes * ln(logZn / 0.69314718056) / 0.69314718056 //parcela que falta para chegar no proximo i
        return (i * samplingIteracoes - parcial).toInt()
    }
}

