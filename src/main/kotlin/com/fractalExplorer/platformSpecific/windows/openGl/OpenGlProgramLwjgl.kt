package com.fractalExplorer.platformSpecific.windows.openGl

import com.fractalExplorer.openGl.OpenGlProgram
import org.lwjgl.opengl.GL32

//todo should this not be an abstract class and start and link shaders
abstract class OpenGlProgramLwjgl : OpenGlProgram {

    final override var programHandle: Int = 0

    override fun useProgram() {
        GL32.glUseProgram(programHandle)
    }

    //todo detalhar melhor erros de compilacao do shader
    protected fun loadShader(type: Int, shaderCode: String): Int {
        return GL32.glCreateShader(type).also { shader ->
            GL32.glShaderSource(shader, shaderCode)
            GL32.glCompileShader(shader)
            println("Shader compile errorcode: ${GL32.glGetError().toString(16)}")
            println(GL32.glGetShaderInfoLog(shader))
        }
    }

    //todo shaders path in constants files

    companion object {
        const val shadersDirectory = "src/main/resources/shaders/"
    }
}