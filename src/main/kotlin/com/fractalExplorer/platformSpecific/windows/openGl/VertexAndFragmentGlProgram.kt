package com.fractalExplorer.platformSpecific.windows.openGl

import com.fractalExplorer.platformSpecific.windows.lerArquivoPC
import org.lwjgl.opengl.GL32 as OGL

//todo: make interface
open class VertexAndFragmentGlProgram(vertexShaderName: String, fragShaderName: String) : OpenGlProgramLwjgl() {

    private val vertexShaderCode = lerArquivoPC(shadersDirectory + vertexShaderName)
    private val fragmentShaderCode = lerArquivoPC(shadersDirectory + fragShaderName)

    init {
        val vertexShader: Int = loadShader(OGL.GL_VERTEX_SHADER, vertexShaderCode)
        val fragmentShader: Int = loadShader(OGL.GL_FRAGMENT_SHADER, fragmentShaderCode)

        programHandle = OGL.glCreateProgram().also {
            if (it == 0) {
                throw RuntimeException("Error creating program.")
            }
            OGL.glAttachShader(it, vertexShader)
            OGL.glAttachShader(it, fragmentShader)
            OGL.glLinkProgram(it)
        }
    }
}