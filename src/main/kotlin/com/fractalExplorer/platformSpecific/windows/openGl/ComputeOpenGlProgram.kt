package com.fractalExplorer.platformSpecific.windows.openGl

import com.fractalExplorer.platformSpecific.windows.lerArquivoPC
import org.lwjgl.opengl.GL43 as OGL

//todo: make interface
class ComputeOpenGlProgram(computeShaderName: String) : OpenGlProgramLwjgl() {

    private val computeShaderCode = lerArquivoPC(shadersDirectory + computeShaderName)

    init {
        val computeShader: Int = loadShader(OGL.GL_COMPUTE_SHADER, computeShaderCode)

        programHandle = OGL.glCreateProgram().also {
            if (it == 0) {
                throw RuntimeException("Error creating openGl program.")
            }
            OGL.glAttachShader(it, computeShader)
            OGL.glLinkProgram(it)
        }
    }

    fun printComputeShaderInfo() {
        val intArray = IntArray(1)

        val workgroupCount = IntArray(3)

        org.lwjgl.opengl.GL43.glGetIntegeri_v(org.lwjgl.opengl.GL43.GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, intArray)
        workgroupCount[0] = intArray[0]
        org.lwjgl.opengl.GL43.glGetIntegeri_v(org.lwjgl.opengl.GL43.GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, intArray)
        workgroupCount[1] = intArray[0]
        org.lwjgl.opengl.GL43.glGetIntegeri_v(org.lwjgl.opengl.GL43.GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, intArray)
        workgroupCount[2] = intArray[0]

        val workgroupSize = IntArray(3)

        org.lwjgl.opengl.GL43.glGetIntegeri_v(org.lwjgl.opengl.GL43.GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, intArray)
        workgroupSize[0] = intArray[0]
        org.lwjgl.opengl.GL43.glGetIntegeri_v(org.lwjgl.opengl.GL43.GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, intArray)
        workgroupSize[1] = intArray[0]
        org.lwjgl.opengl.GL43.glGetIntegeri_v(org.lwjgl.opengl.GL43.GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, intArray)
        workgroupSize[2] = intArray[0]

        org.lwjgl.opengl.GL43.glGetIntegeri_v(org.lwjgl.opengl.GL43.GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, 0, intArray)
        val maxInvocations = intArray[0]

        println("Compute Shader Info:")
        println("Max invocations: $maxInvocations")
        println("Max work Group Size:")
        workgroupSize.forEach {
            println(it)
        }
        println("Max work Group Count:")
        workgroupCount.forEach {
            println(it)
        }
    }
}
