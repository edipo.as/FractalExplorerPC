package com.fractalExplorer.platformSpecific.windows.openGl

import org.lwjgl.opengl.GL32

fun printGlError(name: String = "") {
    var err: Int
    while (GL32.glGetError().also { err = it } != GL32.GL_NO_ERROR) {
        println("GLERROR $name :" + err.toString(16))

    }
}