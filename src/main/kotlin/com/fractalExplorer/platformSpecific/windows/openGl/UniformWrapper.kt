package com.fractalExplorer.platformSpecific.windows.openGl

import com.fractalExplorer.dataTypes.ProgramHandle
import com.fractalExplorer.dataTypes.UniformHandle
import org.lwjgl.opengl.GL32
import java.nio.FloatBuffer

abstract class UniformWrapper(nameInShader: CharSequence, program: ProgramHandle) {

    protected val handle: UniformHandle = GL32.glGetUniformLocation(program, nameInShader)

    fun glGetUniformLocation(program: ProgramHandle, name: CharSequence) = GL32.glGetUniformLocation(program, name)
}

class AttributeWrapper(nameInShader: CharSequence, program: ProgramHandle) {
    private val handle: UniformHandle = GL32.glGetAttribLocation(program, nameInShader)

    fun glVertexAttribPointer(size: Int, type: Int, normalized: Boolean, stride: Int, vertices: FloatBuffer) = GL32.glVertexAttribPointer(handle, size, type, normalized, stride, vertices)
    fun glEnableVertexAttribArray() = GL32.glEnableVertexAttribArray(handle)
}

class Uniform1i(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Int) = GL32.glUniform1i(handle, v0)
}

class Uniform1ui(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Int) = GL32.glUniform1ui(handle, v0)
}

class Uniform1f(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Float) = GL32.glUniform1f(handle, v0)
}

class Uniform2i(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Int, v1: Int) = GL32.glUniform2i(handle, v0, v1)
}

class Uniform2ui(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Int, v1: Int) = GL32.glUniform2ui(handle, v0, v1)
}

class Uniform2f(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Float, v1: Float) = GL32.glUniform2f(handle, v0, v1)
}

class Uniform3i(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Int, v1: Int, v2: Int) = GL32.glUniform3i(handle, v0, v1, v2)
}

class Uniform3ui(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Int, v1: Int, v2: Int) = GL32.glUniform3ui(handle, v0, v1, v2)
}

class Uniform3f(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Float, v1: Float, v2: Float) = GL32.glUniform3f(handle, v0, v1, v2)
}

class Uniform4i(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Int, v1: Int, v2: Int, v3: Int) = GL32.glUniform4i(handle, v0, v1, v2, v3)
}

class Uniform4ui(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Int, v1: Int, v2: Int, v3: Int) = GL32.glUniform4ui(handle, v0, v1, v2, v3)
}

class Uniform4f(nameInShader: CharSequence, program: ProgramHandle) : UniformWrapper(nameInShader, program) {
    fun setValue(v0: Float, v1: Float, v2: Float, v3: Float) = GL32.glUniform4f(handle, v0, v1, v2, v3)
}