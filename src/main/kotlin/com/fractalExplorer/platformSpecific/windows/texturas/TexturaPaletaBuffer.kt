package com.fractalExplorer.platformSpecific.windows.texturas

import com.fractalExplorer.dataTypes.ITexturaGL
import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import org.lwjgl.opengl.GL32

//todo pasar para vetor
class TexturaPaletaBuffer(textureSize: Vetor2i, pixels: TArrayPixels32) : BufferTexture(textureSize, pixels) {

    override var texturaGlEnum: ITexturaGL = idxTexturaGL
    override var idxUniformTextura: ITexturaGL = idxUniformTexturaGL

    override val internalFormat = GL32.GL_RGBA8

    companion object {
        private const val idxTexturaGL: ITexturaGL = GL32.GL_TEXTURE3
        internal const val idxUniformTexturaGL: Int = 3
    }
}