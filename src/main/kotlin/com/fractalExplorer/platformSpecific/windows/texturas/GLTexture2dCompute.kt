package com.fractalExplorer.platformSpecific.windows.texturas

import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.openGl.TextureWrapper
import com.fractalExplorer.platformSpecific.windows.openGl.printGlError
import org.lwjgl.opengl.GL43
import kotlin.math.pow
import org.lwjgl.opengl.GL32 as OGL

//todo create for read only and write only
abstract class GLTexture2dCompute(textureSize: Vetor2i) :
        TextureWrapper(textureSize) {

    private val target = OGL.GL_TEXTURE_2D

    /**check compatibility table 2 https://www.khronos.org/registry/OpenGL-Refpages/es3.0/html/glTexImage2D.xhtml*/
    /** Storage Format, Must be one of: GL_ALPHA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, GL_RGB, GL_RGBA.*/
    open val internalFormat = OGL.GL_RGBA32F

    /** Format of Input data, Must match internalformat */
    open val format = OGL.GL_RGBA

    /** type of Input Data*/
    open val type = OGL.GL_FLOAT

    open val entriesPerPixel = 4

    override fun bind() {
        OGL.glActiveTexture(texturaGlEnum)
        OGL.glBindTexture(
                target,
                textureHandle[0]
        )
    }

    override fun liberarRecursosOGL() {

        OGL.glDeleteTextures(textureHandle)
        possuiTexturaOGLAlocada = false
        texturaCorrespondeAMatrizPixels = false
    }

    override fun createOGLTexture() {
        createTexture()
        //  texturaCorrespondeAMatrizPixels = true
        possuiTexturaOGLAlocada = true

    }

    private fun createTexture() {
        OGL.glGenTextures(textureHandle)

        if (textureHandle[0] != 0) {
            OGL.glActiveTexture(texturaGlEnum)
            OGL.glBindTexture(
                    target,
                    textureHandle[0]
            )
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_WRAP_S, OGL.GL_REPEAT)
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_WRAP_T, OGL.GL_REPEAT)
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_MAG_FILTER, OGL.GL_NEAREST)
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_MIN_FILTER, OGL.GL_NEAREST)
            printGlError("glTexParameteri")
            OGL.glTexImage2D(
                    target,
                    0,
                    internalFormat,
                    textureSize.x,
                    textureSize.y,
                    0,
                    format,
                    type,
                    null as IntArray?
            )
            printGlError("glTexImage2D")
            GL43.glBindImageTexture(
                    0,
                    textureHandle[0],
                    0,
                    false,
                    0,
                    GL43.GL_READ_WRITE,
                    internalFormat
            )
            printGlError("glBindImageTexture")
        } else {
            throw RuntimeException("Error generating Texture OpenGl.")
        }
    }

    fun recoverIntArray(): IntArray {
        val numComponents = textureSize.area() * entriesPerPixel
        val array = IntArray(numComponents)
        GL43.glGetTexImage(
                GL43.GL_TEXTURE_2D,
                0,
                format,
                type,
                array
        )
        val floatArrayB32 = FloatArray(numComponents) {
            (array[it].toDouble() / 2.0.pow(32)).toFloat()
        }
        val floatArrayB30 = FloatArray(numComponents) {
            (array[it].toDouble() / 2.0.pow(30)).toFloat()
        }
        printGlError("glGetTexImage")
        return array
    }

    fun recoverFloatArray(): FloatArray {
        val numComponents = textureSize.area() * entriesPerPixel
        val array = FloatArray(numComponents)
        GL43.glGetTexImage(
                GL43.GL_TEXTURE_2D,
                0,
                format,
                type,
                array
        )
        printGlError("glGetTexImage")
        return array
    }
}
