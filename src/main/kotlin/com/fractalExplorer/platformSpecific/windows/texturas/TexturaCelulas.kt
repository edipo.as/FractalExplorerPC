package com.fractalExplorer.platformSpecific.windows.texturas

import com.fractalExplorer.dataTypes.ITexturaGL
import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import org.lwjgl.opengl.GL32

class TexturaCelulas(textureSize: Vetor2i, pixels: TArrayPixels32) : BufferTexture(textureSize, pixels) {

    override var texturaGlEnum: ITexturaGL = idxTexturaGL
    override var idxUniformTextura: ITexturaGL = idxUniformTexturaGL

    override val internalFormat = GL32.GL_R32UI

    companion object {
        const val idxTexturaGL: ITexturaGL = GL32.GL_TEXTURE2
        const val idxUniformTexturaGL: Int = 2
    }
}