package com.fractalExplorer.platformSpecific.windows.texturas

import com.fractalExplorer.dataTypes.TArrayIteracoes
import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.openGl.TextureWrapper
import org.lwjgl.opengl.GL32

abstract class BufferTexture(textureSize: Vetor2i, pixels: TArrayPixels32) :
        TextureWrapper(textureSize, pixels) {

    abstract val internalFormat: Int

    private var bufferHandle = IntArray(1) { 0 }

    private val target = GL32.GL_TEXTURE_BUFFER

    override fun bind() {
        GL32.glActiveTexture(texturaGlEnum)
        GL32.glBindTexture(
                target,
                textureHandle[0]
        )
    }

    override fun liberarRecursosOGL() {
        GL32.glDeleteBuffers(bufferHandle)
        GL32.glDeleteTextures(textureHandle)
        possuiTexturaOGLAlocada = false
        texturaCorrespondeAMatrizPixels = false
    }

    override fun createOGLTexture() {
        if (iteracoes == null) throw java.lang.RuntimeException("Tentou criar textura OGL sem matriz iteracoes")
        iteracoes?.let {
            createTextureFromBuffer(it)
            texturaCorrespondeAMatrizPixels = true
            possuiTexturaOGLAlocada = true
        }
    }

    private fun createTextureFromBuffer(pixels: TArrayIteracoes) {
        GL32.glGenBuffers(bufferHandle)
        if (bufferHandle[0] != 0) {
            GL32.glActiveTexture(texturaGlEnum)
            GL32.glBindBuffer(
                    target,
                    bufferHandle[0]
            )
            GL32.glBufferData(
                    target,
                    pixels,
                    GL32.GL_STATIC_DRAW
            )
        } else {
            throw RuntimeException("Error generating Buffer OpenGl.")
        }

        GL32.glGenTextures(textureHandle)
        if (textureHandle[0] != 0) {
            GL32.glActiveTexture(texturaGlEnum)
            GL32.glBindTexture(
                    target,
                    textureHandle[0]
            )
        } else {
            throw RuntimeException("Error generating Texture OpenGl.")
        }
        GL32.glTexBuffer(target, internalFormat, bufferHandle[0])
    }
}
