package com.fractalExplorer.platformSpecific.windows.texturas

import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.openGl.TextureWrapper
import com.fractalExplorer.platformSpecific.windows.openGl.printGlError
import org.lwjgl.opengl.GL32 as OGL

//todo caso passe a usar essa classe, talvez unificar com BufferTexture
abstract class GLTexture2D(textureSize: Vetor2i, pixels: TArrayPixels32) :
        TextureWrapper(textureSize, pixels) {

    open val internalFormat = OGL.GL_RGBA
    val format = OGL.GL_RGBA
    val type = OGL.GL_UNSIGNED_BYTE

    private val target = OGL.GL_TEXTURE_2D

    override fun bind() {
        OGL.glActiveTexture(texturaGlEnum)
        OGL.glBindTexture(
                target,
                textureHandle[0]
        )
    }

    override fun liberarRecursosOGL() {

        OGL.glDeleteTextures(textureHandle)
        possuiTexturaOGLAlocada = false
        texturaCorrespondeAMatrizPixels = false
    }

    override fun createOGLTexture() {
        if (iteracoes == null) throw java.lang.RuntimeException("Tentou criar textura OGL sem matriz iteracoes")
        iteracoes?.let {
            createTextureFromArray(it)
            texturaCorrespondeAMatrizPixels = true
            possuiTexturaOGLAlocada = true
        }
    }

    fun createTextureFromArray(pixels: TArrayPixels32) {
        OGL.glGenTextures(textureHandle)

        if (textureHandle[0] != 0) {
            OGL.glActiveTexture(texturaGlEnum)
            OGL.glBindTexture(
                    target,
                    textureHandle[0]
            )
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_WRAP_S, OGL.GL_REPEAT)
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_WRAP_T, OGL.GL_REPEAT)
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_MAG_FILTER, OGL.GL_NEAREST)
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_MIN_FILTER, OGL.GL_NEAREST)
            printGlError("glTexParameteri")
            OGL.glTexImage2D(
                    target,
                    0,
                    internalFormat,
                    textureSize.x,
                    textureSize.y,
                    0,
                    format,
                    type,
                    pixels
            )
            printGlError("glTexImage2D")
        } else {
            throw RuntimeException("Error generating Texture OpenGl.")
        }
    }
}
