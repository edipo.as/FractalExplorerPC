package com.fractalExplorer.platformSpecific.windows.texturas

import com.fractalExplorer.dataTypes.ITexturaGL
import com.fractalExplorer.dataTypes.Vetor2i
import org.lwjgl.opengl.GL32

class TextureComputeInt(textureSize: Vetor2i) : GLTexture2dCompute(textureSize) {

    override var texturaGlEnum: ITexturaGL = idxTexturaGL
    override var idxUniformTextura: ITexturaGL = idxUniformTexturaGL

    override val internalFormat = GL32.GL_RGBA32UI
    override val format = GL32.GL_RGBA
    override val type = GL32.GL_UNSIGNED_INT

    override val entriesPerPixel = 4

    companion object {
        private const val idxTexturaGL: ITexturaGL = GL32.GL_TEXTURE6
        internal const val idxUniformTexturaGL: Int = 6
    }
}