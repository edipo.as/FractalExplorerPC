package com.fractalExplorer.platformSpecific.windows.texturas

import com.fractalExplorer.dataTypes.ITexturaGL
import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import org.lwjgl.opengl.GL32

//todo pasar para vetor
class TexturaPaletaTexture2d(textureSize: Vetor2i, pixels: TArrayPixels32) : GLTexture2D(textureSize, pixels) {

    override var texturaGlEnum: ITexturaGL = idxTexturaGL
    override var idxUniformTextura: ITexturaGL = idxUniformTexturaGL

    //override val internalFormat = GL32.GL_RGBA8

    companion object {
        private const val idxTexturaGL: ITexturaGL = GL32.GL_TEXTURE4
        private const val idxUniformTexturaGL: Int = 4
    }
}