package com.fractalExplorer.platformSpecific.windows.janelas

//todo convert GLFW to my own enums
import com.fractalExplorer.binds.ActionName
import com.fractalExplorer.binds.KeyboardKey
import com.fractalExplorer.dataTypes.*
import com.fractalExplorer.fractal.Fractal
import com.fractalExplorer.fractal.Mandelbrot
import com.fractalExplorer.fractal.MandelbrotOtimizado
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramFractal
import com.fractalExplorer.viewPort.GerenciadorTarefasTexturas
import com.fractalExplorer.viewPort.ViewPortFractal
import com.myGraphicLibrary.glfw.GenericGlfwWindow
import org.lwjgl.glfw.GLFW
import kotlin.math.pow

//todo openGl falha se a janela perde foco

class JanelaPrincipalFractal(val fractal: Fractal = MandelbrotOtimizado()) : GenericGlfwWindow() {
    private val gerenciadorTarefas = GerenciadorTarefasTexturas(relogio)
    internal val viewPortFractal = ViewPortFractal(gerenciadorTarefas, fractal)

    private var posicaoCursorNoPlano = CoordenadasPlano()
    private var glProgram = GlProgramFractal(viewPortFractal)

    var waypoints = listOf<CameraPosition>(
        CameraPosition(CoordenadasPlano(-1.7495738600330935, -0.0001095831709530), 1.0 / 95744500.800000),
        CameraPosition(CoordenadasPlano(-0.1606636799927043, 1.0337425760632901), 1.0 / 160.459118),
        CameraPosition(CoordenadasPlano(-1.0284578028870617, 0.3614638719357337), 1.0 / 52858348310.5402069),
        CameraPosition(CoordenadasPlano(0.3636746748019200, -0.0770936509878735), 1.0 / 1956347819.068485),
        CameraPosition(CoordenadasPlano(-1.0284578100619959, 0.3614639337444190), 1.0 / 1914436079.013541),
        CameraPosition(CoordenadasPlano(-1.2639632011873858, 0.3832794703861065), 1.0 / 28308897713.248013)
    )

    val nwaypoints = waypoints.size

    var waypointIdx = 0
        set(value) {
            field = (value + nwaypoints) % nwaypoints
        }


    init {
        addDefaultBindings()
        addActions()

        glProgram.useProgram()

        viewPortFractal.camera.dimensaoJanelaSaida = dimensoesJanela.toCoordenadasTelaF()

        println("INIT JanelaPrincipalFractal")
        alterarTitulo("Mandelbrot!")
        configuraAcoesMouse()
        //todo tirar daqui
        //bindPaleta()
        glProgram.bindProximaPaleta()
    }

    private fun addDefaultBindings() {
        keyBinds[KeyboardKey.KEY_SPACE] = ActionName.CAMERA_ZOOM_UP
        keyBinds[KeyboardKey.KEY_Z] = ActionName.CAMERA_ZOOM_DOWN
        keyBinds[KeyboardKey.KEY_A] = ActionName.CAMERA_PAN_LEFT
        keyBinds[KeyboardKey.KEY_D] = ActionName.CAMERA_PAN_RIGHT
        keyBinds[KeyboardKey.KEY_W] = ActionName.CAMERA_TILT_UP
        keyBinds[KeyboardKey.KEY_S] = ActionName.CAMERA_TILT_DOWN
        keyBinds[KeyboardKey.KEY_LEFT] = ActionName.CAMERA_PAN_LEFT
        keyBinds[KeyboardKey.KEY_RIGHT] = ActionName.CAMERA_PAN_RIGHT
        keyBinds[KeyboardKey.KEY_UP] = ActionName.CAMERA_TILT_UP
        keyBinds[KeyboardKey.KEY_DOWN] = ActionName.CAMERA_TILT_DOWN
        keyBinds[KeyboardKey.KEY_KP_4] = ActionName.PALETTE_DENSITY_DEC
        keyBinds[KeyboardKey.KEY_KP_6] = ActionName.PALETTE_DENSITY_INC
        keyBinds[KeyboardKey.KEY_KP_5] = ActionName.PALETTE_MOVEMENT_TOGGLE
        keyBinds[KeyboardKey.KEY_KP_8] = ActionName.PALETTE_SPEED_INC
        keyBinds[KeyboardKey.KEY_KP_2] = ActionName.PALETTE_SPEED_DEC
        keyBinds[KeyboardKey.KEY_KP_7] = ActionName.PALETTE_POSITION_OFFSET_INC
        keyBinds[KeyboardKey.KEY_KP_9] = ActionName.PALETTE_POSITION_OFFSET_DEC
        keyBinds[KeyboardKey.KEY_KP_1] = ActionName.DEBUG_GENERIC_INC
        keyBinds[KeyboardKey.KEY_KP_3] = ActionName.DEBUG_GENERIC_DEC
        keyBinds[KeyboardKey.KEY_N] = ActionName.PALETTE_NORMALIZATION_TOGGLE
        keyBinds[KeyboardKey.KEY_R] = ActionName.CAMERA_ROTATION_INC
        keyBinds[KeyboardKey.KEY_T] = ActionName.CAMERA_ROTATION_DEC
        keyBinds[KeyboardKey.KEY_E] = ActionName.PALETTE_BIND_INC
        keyBinds[KeyboardKey.KEY_Q] = ActionName.PALETTE_BIND_DEC
        keyBinds[KeyboardKey.KEY_J] = ActionName.NEW_WINDOW_JULIA_SET
        keyBinds[KeyboardKey.KEY_O] = ActionName.WAYPOINT_INC
        keyBinds[KeyboardKey.KEY_P] = ActionName.WAYPOINT_DEC
    }

    private fun addActions() {
        with(viewPortFractal) {
            actionBinds[ActionName.CAMERA_ZOOM_UP] = { camera.zoomPosDesejada(mutableProperties.fatorEscalaDesktop) }
            actionBinds[ActionName.CAMERA_ZOOM_DOWN] =
                { camera.zoomPosDesejada(1 / mutableProperties.fatorEscalaDesktop) }
            actionBinds[ActionName.CAMERA_PAN_LEFT] = { camera.moverPosDesejada(-1f, 0f) }
            actionBinds[ActionName.CAMERA_PAN_RIGHT] = { camera.moverPosDesejada(+1f, 0f) }
            actionBinds[ActionName.CAMERA_TILT_UP] = { camera.moverPosDesejada(0f, -1f) }
            actionBinds[ActionName.CAMERA_TILT_DOWN] = { camera.moverPosDesejada(0f, +1f) }
            actionBinds[ActionName.CAMERA_PAN_LEFT] = { camera.moverPosDesejada(-1f, 0f) }
            actionBinds[ActionName.CAMERA_PAN_RIGHT] = { camera.moverPosDesejada(+1f, 0f) }
            actionBinds[ActionName.CAMERA_TILT_UP] = { camera.moverPosDesejada(0f, -1f) }
            actionBinds[ActionName.CAMERA_TILT_DOWN] = { camera.moverPosDesejada(0f, +1f) }
            actionBinds[ActionName.CAMERA_ROTATION_INC] = { camera.pos.desejada.angulo += 30.0 }
            actionBinds[ActionName.CAMERA_ROTATION_DEC] = { camera.pos.desejada.angulo -= 30.0 }
            actionBinds[ActionName.PALETTE_DENSITY_DEC] = { mutableProperties.iteracoesPorPaleta *= 1.5 }
            actionBinds[ActionName.PALETTE_DENSITY_INC] = { mutableProperties.iteracoesPorPaleta /= 1.5 }
            actionBinds[ActionName.PALETTE_MOVEMENT_TOGGLE] =
                { mutableProperties.circularCores = !mutableProperties.circularCores }
            actionBinds[ActionName.PALETTE_SPEED_INC] = { mutableProperties.velocidadeCircularCores += 0.01 }
            actionBinds[ActionName.PALETTE_SPEED_DEC] = { mutableProperties.velocidadeCircularCores -= 0.01 }
            actionBinds[ActionName.PALETTE_NORMALIZATION_TOGGLE] =
                { mutableProperties.reverterNormalizacao = !mutableProperties.reverterNormalizacao }
            actionBinds[ActionName.PALETTE_POSITION_OFFSET_INC] = { glProgram.gerenciadorPaletas.offsetPaleta -= 0.1 }
            actionBinds[ActionName.PALETTE_POSITION_OFFSET_DEC] = { glProgram.gerenciadorPaletas.offsetPaleta += 0.1 }
            actionBinds[ActionName.PALETTE_BIND_INC] = { glProgram.bindProximaPaleta() }
            actionBinds[ActionName.PALETTE_BIND_DEC] = { glProgram.bindAnteriorPaleta() }
            //todo julia start awkward
            actionBinds[ActionName.NEW_WINDOW_JULIA_SET] =
                { if (fractal is Mandelbrot) fractal.coordenadasJulia = posicaoCursorNoPlano }
            actionBinds[ActionName.DEBUG_GENERIC_INC] = { offsetDebug++ }
            actionBinds[ActionName.DEBUG_GENERIC_DEC] = { offsetDebug-- }
            actionBinds[ActionName.WAYPOINT_INC] = {
                waypointIdx++
                camera.pos.desejada = waypoints[waypointIdx]
            }
            actionBinds[ActionName.WAYPOINT_DEC] = {
                waypointIdx--
                camera.pos.desejada = waypoints[waypointIdx]
            }
        }
    }

    override fun aCadaQuadroGlfw() {
        posicaoCursorNoPlano =
            viewPortFractal.camera.posicaoCursorNaTela(CoordenadasTelaI(mouse.x, mouse.y).toCoordenadasTelaF())
                .toCoordenadasPlano(viewPortFractal.camera.pos.atual)
        alterarTitulo("Mandelbrot ! Posicao: ${posicaoCursorNoPlano.toStringFormat(15)}")

        glProgram.renderFrame()

        gerenciadorTarefas.executaTarefasTexturas(15)
    }

    override fun acaoAoRedimensionar(janela: GLFWWindowHandle, windowSize: Vetor2i) {
        viewPortFractal.camera.dimensaoJanelaSaida = windowSize.toVetor2d()
        glProgram.viewPortSize = windowSize
    }

    private fun configuraAcoesMouse() {
        val fatorZoomTeclas = 0.5
        val fatorZoomScroll = 0.2
        mouse.scrollAction = { offset: Double, x: Int, y: Int ->
            viewPortFractal.camera.zoomPosDesejadaPontoFixo((2.0).pow(offset * fatorZoomScroll), CoordenadasTelaI(x, y).toCoordenadasTelaF())
        }
        mouse.botoes[GLFW.GLFW_MOUSE_BUTTON_1]?.clickAction = { x: Int, y: Int ->
            viewPortFractal.camera.zoomPosDesejadaPontoFixo(fatorZoomTeclas * 2, CoordenadasTelaI(x, y).toCoordenadasTelaF())
        }
        mouse.botoes[GLFW.GLFW_MOUSE_BUTTON_1]?.dragAction = { dx: Int, dy: Int ->
            viewPortFractal.camera.moverPosAtual(CoordenadasTelaI(-dx, -dy).toCoordenadasTelaF())
        }
        mouse.botoes[GLFW.GLFW_MOUSE_BUTTON_2]?.clickAction = { x: Int, y: Int ->
            viewPortFractal.camera.zoomPosDesejadaPontoFixo(fatorZoomTeclas * 1 / 2, CoordenadasTelaI(x, y).toCoordenadasTelaF())
        }
    }

    override fun aoFecharJanela() {
        viewPortFractal.liberarRecursos()
    }
}
