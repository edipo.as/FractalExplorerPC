package com.fractalExplorer.platformSpecific.windows

import com.fractalExplorer.interfaces.Relogio

class Relogio : Relogio {
    override fun getCurrentTimeMs(): Long {
        return System.currentTimeMillis()
    }
}