package com.fractalExplorer.platformSpecific.windows

import com.fractalExplorer.fractal.Julia
import com.fractalExplorer.fractal.Mandelbrot
import com.fractalExplorer.platformSpecific.windows.janelas.JanelaPrincipalFractal
import com.fractalExplorer.threads.ThreadProcessamento
import com.myGraphicLibrary.glfw.GerenciadorDeJanelasGLFW

//todo make it platform agnostic
class WindowsApplication : GerenciadorDeJanelasGLFW() {

    private val numThreadsProcessamento: Int = Runtime.getRuntime().availableProcessors()//todo make configurable
    private val threadsProcessamento = List(numThreadsProcessamento) { ThreadProcessamento(this) }


    init {
        threadsProcessamento.forEach { it.start() }
    }

    val janelasFractal: List<JanelaPrincipalFractal>
        get() = threadsOpenGl
                .map { it.glfwWindow }
                .filterIsInstance<JanelaPrincipalFractal>()


    override fun emCadaQuadro() {
        janelasFractal.forEach { it ->
            if (it.fractal is Mandelbrot) {
                it.fractal.coordenadasJulia?.let { addJanela { JanelaPrincipalFractal(Julia(it)) } }
                it.fractal.coordenadasJulia = null
            }
        }
    }

    override fun aoEncerrar() {
        threadsProcessamento.forEach {
            it.interrupt()
        }
    }
}
