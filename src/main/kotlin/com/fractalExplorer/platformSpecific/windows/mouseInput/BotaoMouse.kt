package com.fractalExplorer.platformSpecific.windows.mouseInput

import com.fractalExplorer.dataTypes.IBotaoMouse
import org.lwjgl.glfw.GLFW

class BotaoMouse(val botao: IBotaoMouse) {
    var estado = EstadoBotaoMouse.FREE
        private set

    var clickAction: ((x: Int, y: Int) -> Unit)? = null
    var dragAction: ((dx: Int, dy: Int) -> Unit)? = null


    fun handleAction(action: Int, x: Int, y: Int) {
        when (action) {
            GLFW.GLFW_PRESS -> {
                if (estado == EstadoBotaoMouse.FREE) {
                    estado = EstadoBotaoMouse.HOLD
                }
            }
            GLFW.GLFW_RELEASE -> {
                if (estado == EstadoBotaoMouse.HOLD) {
                    estado = EstadoBotaoMouse.FREE
                    clickAction?.invoke(x, y)
                }
                if (estado == EstadoBotaoMouse.DRAG) {
                    estado = EstadoBotaoMouse.FREE
                }
            }
        }
    }

    fun handleMovement(dx: Int, dy: Int) {
        if (estado == EstadoBotaoMouse.HOLD) {
            estado = EstadoBotaoMouse.DRAG
        }

        if (estado == EstadoBotaoMouse.DRAG) {
            dragAction?.invoke(dx, dy)
        }
    }
}