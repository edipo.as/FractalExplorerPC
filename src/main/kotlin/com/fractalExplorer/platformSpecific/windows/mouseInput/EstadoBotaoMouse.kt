package com.fractalExplorer.platformSpecific.windows.mouseInput

enum class EstadoBotaoMouse {
    HOLD, DRAG, FREE
}