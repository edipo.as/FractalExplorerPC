package com.fractalExplorer.platformSpecific.windows.mouseInput

import com.fractalExplorer.dataTypes.IAcaoBotao
import com.fractalExplorer.dataTypes.IBotaoMouse

open class MouseWrapper(iBotoes: List<IBotaoMouse>) {
    var scrollAction: ((offset: Double, x: Int, y: Int) -> Unit)? = null

    //todo transform in vetor
    var x = 0
    var y = 0

    var oldx = 0
    var oldy = 0

    var botoes = iBotoes.map {
        Pair(it, BotaoMouse(it))
    }.toMap()


    fun handleMouseButton(botao: IBotaoMouse, action: IAcaoBotao) {
        botoes.values.forEach {
            if (botao == it.botao) {
                it.handleAction(action, x, y)
            }
        }
    }

    fun handleMouseMovement(X: Int, Y: Int) {
        val dx = X - x
        val dy = Y - y

        oldx = x
        oldy = y

        x = X
        y = Y

        botoes.values.forEach {
            it.handleMovement(dx, dy)
        }
    }

    fun handleScrollAction(offset: Double) {
        scrollAction?.invoke(offset, x, y)
    }
}
