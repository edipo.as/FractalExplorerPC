package com.fractalExplorer.platformSpecific.windows

import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.interfaces.GerenciadorDeImplementacoesInterface
import com.fractalExplorer.interfaces.Relogio
import com.fractalExplorer.openGl.TextureWrapper
import com.fractalExplorer.platformSpecific.windows.texturas.TexturaCelulas
import com.fractalExplorer.platformSpecific.windows.texturas.TexturaPaletaBuffer

class GerenciadorDeImplementacoes {
    companion object : GerenciadorDeImplementacoesInterface {
        override fun relogio(): Relogio = Relogio()
        override fun texturaCelula(textureSize: Vetor2i, pixels: TArrayPixels32): TextureWrapper = TexturaCelulas(textureSize, pixels)
        override fun texturaPaleta(textureSize: Vetor2i, pixels: TArrayPixels32): TextureWrapper = TexturaPaletaBuffer(textureSize, pixels)
    }
}