package com.fractalExplorer.platformSpecific.windows.glProgram

import com.fractalExplorer.dataTypes.Vetor2d
import com.fractalExplorer.openGl.GlProgramSprite
import com.fractalExplorer.openGl.Sprite
import com.fractalExplorer.platformSpecific.windows.openGl.AttributeWrapper
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform2f
import org.lwjgl.opengl.GL32 as OGL

//todo esta classe deve cuidar dos uniforms??
open class GlProgramSpriteBufferTexture(
        fragmentShader: String = "sprite/Fragment.glsl"
) : GlProgramSprite, GlProgramRectangleVertex("sprite/Vertex.glsl", fragmentShader) {

    val attributeVertexPosition = AttributeWrapper("a_RectangleVertices", programHandle)
    val uniformSpritePosition = Uniform2f("u_SpritePosition", programHandle)
    val uniformSpriteSize = Uniform2f("u_SpriteSize", programHandle)
    private val uniformRotationSinCos = Uniform2f("u_rotacaoCosSen", programHandle)

    //todo criar metodo para desenhar retangulos
    override fun desenharSpriteQuadrado(sprite: Sprite, tamSprite: Vetor2d) {
        sprite.bindTexture()
        uniformSpritePosition.setValue(sprite.posAbsoluteToParent.x.toFloat(), -sprite.posAbsoluteToParent.y.toFloat())
        uniformSpriteSize.setValue(tamSprite.x.toFloat(), tamSprite.y.toFloat())
        mVertices.position(mPositionOffset)
        attributeVertexPosition.glVertexAttribPointer(
                mValuesPerVertice, OGL.GL_FLOAT, false,
                mStrideBytes, mVertices
        )
        attributeVertexPosition.glEnableVertexAttribArray()
        OGL.glDrawArrays(OGL.GL_TRIANGLE_STRIP, 0, 4)
    }
}
