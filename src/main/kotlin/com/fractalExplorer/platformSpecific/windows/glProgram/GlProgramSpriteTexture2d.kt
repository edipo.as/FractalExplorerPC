package com.fractalExplorer.platformSpecific.windows.glProgram

import com.fractalExplorer.dataTypes.Vetor2d
import com.fractalExplorer.openGl.GlProgramSprite
import com.fractalExplorer.openGl.Sprite
import com.fractalExplorer.platformSpecific.windows.openGl.AttributeWrapper
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform2f
import org.lwjgl.opengl.GL32 as OGL

//todo esta classe deve cuidar dos uniforms??
open class GlProgramSpriteTexture2d(
    fragmentShader: String = "basicFragments/FragmentTexture2d.glsl"
) : GlProgramSprite, GlProgramRectangleVertex("sprite/Vertex.glsl", fragmentShader) {

    private val attributeVertexPosition = AttributeWrapper("a_RectangleVertices", programHandle)
    private val uniformSpritePositionAbs = Uniform2f("u_SpritePosition", programHandle)
    private val uniformSpritePositionRel = Uniform2f("u_SpriteRelativePosition", programHandle)
    private val uniformSpriteSizeRelative = Uniform2f("u_SpriteSizeRelative", programHandle)
    private val uniformSpriteSizeAbs = Uniform2f("u_SpriteSize", programHandle)
    private val uniformRotationSinCos = Uniform2f("u_rotacaoCosSen", programHandle)

    val sprites = emptyList<Sprite>().toMutableList()


    fun drawAll() {
        sprites.forEach { sprite ->

            currentTextureSize = sprite.textura.textureSize
            sprite.bindTexture()
            uniformSpritePositionAbs.setValue(sprite.posAbsolute.x.toFloat(), -sprite.posAbsolute.y.toFloat())
            uniformSpritePositionRel.setValue(sprite.posRelative.x.toFloat(), -sprite.posRelative.y.toFloat())
            uniformSpriteSizeAbs.setValue(sprite.sizeAbsolute.x.toFloat(), sprite.sizeAbsolute.y.toFloat())
            uniformSpriteSizeRelative.setValue(sprite.sizeRelative.x.toFloat(), sprite.sizeRelative.y.toFloat())
            uniformRotationSinCos.setValue(sprite.rotation.cos(), sprite.rotation.sen())

            mVertices.position(mPositionOffset)
            attributeVertexPosition.glVertexAttribPointer(
                mValuesPerVertice, OGL.GL_FLOAT, false,
                mStrideBytes, mVertices
            )

            attributeVertexPosition.glEnableVertexAttribArray()
            OGL.glDrawArrays(OGL.GL_TRIANGLE_STRIP, 0, 4)
        }
    }

    fun drawRecursive(sprite: Sprite) {

        if (!sprite.invisible) {
            currentTextureSize = sprite.textura.textureSize
            sprite.bindTexture()
            uniformSpritePositionAbs.setValue(sprite.posAbsolute.x.toFloat(), -sprite.posAbsolute.y.toFloat())
            uniformSpritePositionRel.setValue(sprite.posRelative.x.toFloat(), -sprite.posRelative.y.toFloat())
            uniformSpriteSizeAbs.setValue(sprite.sizeAbsolute.x.toFloat(), sprite.sizeAbsolute.y.toFloat())
            uniformSpriteSizeRelative.setValue(sprite.sizeRelative.x.toFloat(), sprite.sizeRelative.y.toFloat())
            uniformRotationSinCos.setValue(sprite.rotation.cos(), sprite.rotation.sen())

            mVertices.position(mPositionOffset)
            attributeVertexPosition.glVertexAttribPointer(
                mValuesPerVertice, OGL.GL_FLOAT, false,
                mStrideBytes, mVertices
            )

            attributeVertexPosition.glEnableVertexAttribArray()
            OGL.glDrawArrays(OGL.GL_TRIANGLE_STRIP, 0, 4)
        }
        sprite.childs.forEach {
            drawRecursive(it)
        }

    }

    //todo criar metodo para desenhar retangulos
    override fun desenharSpriteQuadrado(sprite: Sprite, tamSprite: Vetor2d) {
        sprite.bindTexture()
        uniformSpritePositionAbs.setValue(sprite.posAbsolute.x.toFloat(), -sprite.posAbsolute.y.toFloat())
        uniformSpriteSizeAbs.setValue(tamSprite.x.toFloat(), tamSprite.y.toFloat())
        uniformSpritePositionRel.setValue(sprite.posRelative.x.toFloat(), -sprite.posRelative.y.toFloat())
        uniformSpriteSizeRelative.setValue(sprite.sizeRelative.x.toFloat(), sprite.sizeRelative.y.toFloat())
        uniformRotationSinCos.setValue(sprite.rotation.cos(), sprite.rotation.sen())

        mVertices.position(mPositionOffset)
        attributeVertexPosition.glVertexAttribPointer(
            mValuesPerVertice, OGL.GL_FLOAT, false,
            mStrideBytes, mVertices
        )
        attributeVertexPosition.glEnableVertexAttribArray()
        OGL.glDrawArrays(OGL.GL_TRIANGLE_STRIP, 0, 4)
    }

}
