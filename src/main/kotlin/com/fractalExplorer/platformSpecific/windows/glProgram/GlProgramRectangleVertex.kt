package com.fractalExplorer.platformSpecific.windows.glProgram

import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.openGl.TextureWrapper
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform1i
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform2f
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform2i
import com.fractalExplorer.platformSpecific.windows.openGl.VertexAndFragmentGlProgram
import java.nio.ByteBuffer.allocateDirect
import java.nio.ByteOrder
import org.lwjgl.opengl.GL32 as OGL

abstract class GlProgramRectangleVertex(
        vertexShader: String,
        fragmentShader: String
) : VertexAndFragmentGlProgram(
        vertexShader,
        fragmentShader
) {

    //todo make all glprogrmas share same uniform
    var viewPortSize = Vetor2i(0, 0)
        set(value) {
            field = value
            useProgram()
            uniformViewportSize.setValue(value.x.toFloat(), value.y.toFloat())
            glViewport(0, 0, value.x, value.y)
        }

    var currentTextureSize = Vetor2i(0, 0)
        set(value) {
            field = value
            useProgram()
            uniformTextureSize.setValue(currentTextureSize.x, currentTextureSize.y)
        }

    var currentTextureIdxUniform = 0
        set(value) {
            field = value
            useProgram()
            uniformTextura.setValue(value)
        }

    private val uniformTextureSize = Uniform2i("u_TextureSize", programHandle)
    private val uniformViewportSize = Uniform2f("u_ViewportSize", programHandle)
    private val uniformTextura = Uniform1i("u_Textura", programHandle)


    init {
        viewPortSize = glGetViewport()
    }

    //todo make constructor???
    fun copySetupFromTexture(texture: TextureWrapper) {
        currentTextureSize = texture.textureSize
        currentTextureIdxUniform = texture.idxUniformTextura
    }

    fun glViewport(x: Int, y: Int, width: Int, height: Int) = OGL.glViewport(x, y, width, height)

    fun glGetViewport(): Vetor2i {
        val viewport = IntArray(4)        //x, y, height, width
        OGL.glGetIntegerv(OGL.GL_VIEWPORT, viewport)
        return Vetor2i(viewport[2], viewport[3])
    }


    companion object {
        private const val mBytesPerFloat = 4
        internal const val mStrideBytes = 2 * mBytesPerFloat
        internal const val mPositionOffset = 0
        internal const val mValuesPerVertice = 2

        private val verticesQuadrado = floatArrayOf(
                // X, Y, Z,
                0.0f, 0.0f,
                0.0f, -1.0f,
                1.0f, 0.0f,
                1.0f, -1.0f
        )
        internal val mVertices = allocateDirect(verticesQuadrado.size * mBytesPerFloat)
                .order(ByteOrder.nativeOrder()).asFloatBuffer()
                .apply {
                    put(verticesQuadrado)!!.position(0)
                }
    }
}

