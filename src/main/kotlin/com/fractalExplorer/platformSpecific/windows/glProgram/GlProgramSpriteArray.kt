package com.fractalExplorer.platformSpecific.windows.glProgram

import com.fractalExplorer.dataTypes.Angle
import com.fractalExplorer.dataTypes.CoordenadasTelaF
import com.fractalExplorer.dataTypes.Vetor2d
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.openGl.Sprite
import com.fractalExplorer.openGl.glProgramSpriteArrayOpenGL
import com.fractalExplorer.platformSpecific.windows.openGl.AttributeWrapper
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform2f
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform2i
import org.lwjgl.opengl.GL32 as OGL

//todo esta classe deve cuidar dos uniforms??
open class GlProgramSpriteArray(
        fragmentShader: String = "fractal/Fragment.glsl"
) : glProgramSpriteArrayOpenGL, GlProgramRectangleVertex("fractal/Vertex.glsl", fragmentShader) {


    private val attributeVertexPosition = AttributeWrapper("a_RectangleVertices", programHandle)
    private val uniformSpritePosition = Uniform2f("u_SpritePosition", programHandle)
    private val uniformSpriteSize = Uniform2f("u_SpriteSize", programHandle)
    private val uniformIndiceRelativoCelulas = Uniform2i("u_indiceRelativoCelula", programHandle)
    private val uniformRotationSinCos = Uniform2f("u_rotacaoCosSen", programHandle)

    override fun desenharSpriteArray(celula: Sprite, tamSprite: Vetor2d, angle: Angle, indice: Vetor2i, coordenadasCantoSupEsq: CoordenadasTelaF) {
        celula.bindTexture()
        uniformSpritePosition.setValue(coordenadasCantoSupEsq.x.toFloat(), -coordenadasCantoSupEsq.y.toFloat())
        uniformSpriteSize.setValue(tamSprite.x.toFloat(), tamSprite.y.toFloat())
        uniformIndiceRelativoCelulas.setValue(indice.x, indice.y)
        uniformRotationSinCos.setValue(angle.cos(), angle.sen())
        //  println("renderizou x${celula.indiceRelativoNaCamada.x} y${celula.indiceRelativoNaCamada.y}")

        mVertices.position(mPositionOffset)

        attributeVertexPosition.glVertexAttribPointer(
                mValuesPerVertice, OGL.GL_FLOAT, false,
                mStrideBytes, mVertices
        )
        attributeVertexPosition.glEnableVertexAttribArray()
        OGL.glDrawArrays(OGL.GL_TRIANGLE_STRIP, 0, 4)
    }
}
