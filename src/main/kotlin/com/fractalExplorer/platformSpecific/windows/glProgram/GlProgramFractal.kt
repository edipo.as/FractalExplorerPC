package com.fractalExplorer.platformSpecific.windows.glProgram

import com.fractalExplorer.dataTypes.TimeMillisecondsI
import com.fractalExplorer.dataTypes.toCppStyleInt
import com.fractalExplorer.paletas.GerenciadorPaletas
import com.fractalExplorer.platformSpecific.windows.Relogio
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform1f
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform1i
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform1ui
import com.fractalExplorer.platformSpecific.windows.texturas.TexturaCelulas
import com.fractalExplorer.platformSpecific.windows.texturas.TexturaPaletaBuffer
import com.fractalExplorer.viewPort.ViewPortFractal

class GlProgramFractal(val viewPortFractal: ViewPortFractal) : GlProgramSpriteArray() {

    val relogio = Relogio()

    var instanteDoQuadroAnterior: TimeMillisecondsI = relogio.getCurrentTimeMs()
    val gerenciadorPaletas = GerenciadorPaletas(viewPortFractal)

    private val uniformIterations = Uniform1i("u_Iteracoes", programHandle)
    private val uniformPalette = Uniform1i("u_Paleta", programHandle)
    private val uniformPaletteOffset = Uniform1f("u_OffsetPaleta", programHandle)
    private val uniformNumColorsPalette = Uniform1i("u_qtdeCoresPaleta", programHandle)
    private val uniformIterationsPerPalette = Uniform1ui("u_IteracoesPorPaleta", programHandle)
    private val uniformUndoNormalization = Uniform1ui("u_NaoNormalizar", programHandle)
    private val uniformMaxIterations = Uniform1ui("u_MaxIteracoes", programHandle)
    private val uniformDebug = Uniform1i("u_Offset", programHandle)
    private val uniformSamplesPerIteration = Uniform1ui("u_SamplesPorIteracao", programHandle)

    init {
        useProgram()
        currentTextureSize = viewPortFractal.immutableProperties.tamSprite
        uniformIterations.setValue(TexturaCelulas.idxUniformTexturaGL)
        uniformSamplesPerIteration.setValue(viewPortFractal.mutableProperties.samplesPorIteracao)
        uniformPalette.setValue(TexturaPaletaBuffer.idxUniformTexturaGL)
        uniformMaxIterations.setValue(viewPortFractal.mutableProperties.maxIteracoes)
    }

    fun renderFrame() {

        val instanteAtual = relogio.getCurrentTimeMs()
        val incrementoTempo = instanteAtual - instanteDoQuadroAnterior //millis
        instanteDoQuadroAnterior = instanteAtual
        gerenciadorPaletas.atualizarPosicaoPaleta(incrementoTempo)

        useProgram()
        uniformsPerFrame()
        viewPortFractal.desenharFrame(this)
    }

    private fun uniformsPerFrame() {
        uniformDebug.setValue(viewPortFractal.offsetDebug)
        uniformPaletteOffset.setValue(gerenciadorPaletas.offsetPaleta.toFloat())
        uniformUndoNormalization.setValue(viewPortFractal.mutableProperties.reverterNormalizacao.toCppStyleInt())
        uniformIterationsPerPalette.setValue(viewPortFractal.mutableProperties.iteracoesPorPaleta.toInt())
    }

    internal fun bindProximaPaleta() {
        //     GLFW.glfwMakeContextCurrent(windowHandle)
        gerenciadorPaletas.bindNext()
        uniformNumColorsPalette.setValue(gerenciadorPaletas.getTamPaletaAtual())
    }

    internal fun bindAnteriorPaleta() {
        //   GLFW.glfwMakeContextCurrent(windowHandle)
        gerenciadorPaletas.bindAnterior()
        uniformNumColorsPalette.setValue(gerenciadorPaletas.getTamPaletaAtual())

    }
}