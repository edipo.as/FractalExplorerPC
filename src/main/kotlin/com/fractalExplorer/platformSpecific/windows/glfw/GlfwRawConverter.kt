package com.fractalExplorer.platformSpecific.windows.glfw

import com.fractalExplorer.binds.KeyboardKey
import com.fractalExplorer.dataTypes.TeclaGLFW
import org.lwjgl.glfw.GLFW


class GlfwRawConverter {
    companion object {
        fun convertRawKey(teclaGLFW: TeclaGLFW): KeyboardKey {
            return when (teclaGLFW) {
                GLFW.GLFW_KEY_SPACE -> KeyboardKey.KEY_SPACE
                GLFW.GLFW_KEY_APOSTROPHE -> KeyboardKey.KEY_APOSTROPHE
                GLFW.GLFW_KEY_COMMA -> KeyboardKey.KEY_COMMA
                GLFW.GLFW_KEY_MINUS -> KeyboardKey.KEY_MINUS
                GLFW.GLFW_KEY_PERIOD -> KeyboardKey.KEY_PERIOD
                GLFW.GLFW_KEY_SLASH -> KeyboardKey.KEY_SLASH
                GLFW.GLFW_KEY_0 -> KeyboardKey.KEY_0
                GLFW.GLFW_KEY_1 -> KeyboardKey.KEY_1
                GLFW.GLFW_KEY_2 -> KeyboardKey.KEY_2
                GLFW.GLFW_KEY_3 -> KeyboardKey.KEY_3
                GLFW.GLFW_KEY_4 -> KeyboardKey.KEY_4
                GLFW.GLFW_KEY_5 -> KeyboardKey.KEY_5
                GLFW.GLFW_KEY_6 -> KeyboardKey.KEY_6
                GLFW.GLFW_KEY_7 -> KeyboardKey.KEY_7
                GLFW.GLFW_KEY_8 -> KeyboardKey.KEY_8
                GLFW.GLFW_KEY_9 -> KeyboardKey.KEY_9
                GLFW.GLFW_KEY_SEMICOLON -> KeyboardKey.KEY_SEMICOLON
                GLFW.GLFW_KEY_EQUAL -> KeyboardKey.KEY_EQUAL
                GLFW.GLFW_KEY_A -> KeyboardKey.KEY_A
                GLFW.GLFW_KEY_B -> KeyboardKey.KEY_B
                GLFW.GLFW_KEY_C -> KeyboardKey.KEY_C
                GLFW.GLFW_KEY_D -> KeyboardKey.KEY_D
                GLFW.GLFW_KEY_E -> KeyboardKey.KEY_E
                GLFW.GLFW_KEY_F -> KeyboardKey.KEY_F
                GLFW.GLFW_KEY_G -> KeyboardKey.KEY_G
                GLFW.GLFW_KEY_H -> KeyboardKey.KEY_H
                GLFW.GLFW_KEY_I -> KeyboardKey.KEY_I
                GLFW.GLFW_KEY_J -> KeyboardKey.KEY_J
                GLFW.GLFW_KEY_K -> KeyboardKey.KEY_K
                GLFW.GLFW_KEY_L -> KeyboardKey.KEY_L
                GLFW.GLFW_KEY_M -> KeyboardKey.KEY_M
                GLFW.GLFW_KEY_N -> KeyboardKey.KEY_N
                GLFW.GLFW_KEY_O -> KeyboardKey.KEY_O
                GLFW.GLFW_KEY_P -> KeyboardKey.KEY_P
                GLFW.GLFW_KEY_Q -> KeyboardKey.KEY_Q
                GLFW.GLFW_KEY_R -> KeyboardKey.KEY_R
                GLFW.GLFW_KEY_S -> KeyboardKey.KEY_S
                GLFW.GLFW_KEY_T -> KeyboardKey.KEY_T
                GLFW.GLFW_KEY_U -> KeyboardKey.KEY_U
                GLFW.GLFW_KEY_V -> KeyboardKey.KEY_V
                GLFW.GLFW_KEY_W -> KeyboardKey.KEY_W
                GLFW.GLFW_KEY_X -> KeyboardKey.KEY_X
                GLFW.GLFW_KEY_Y -> KeyboardKey.KEY_Y
                GLFW.GLFW_KEY_Z -> KeyboardKey.KEY_Z
                GLFW.GLFW_KEY_LEFT_BRACKET -> KeyboardKey.KEY_LEFT_BRACKET
                GLFW.GLFW_KEY_BACKSLASH -> KeyboardKey.KEY_BACKSLASH
                GLFW.GLFW_KEY_RIGHT_BRACKET -> KeyboardKey.KEY_RIGHT_BRACKET
                GLFW.GLFW_KEY_GRAVE_ACCENT -> KeyboardKey.KEY_GRAVE_ACCENT
                GLFW.GLFW_KEY_WORLD_1 -> KeyboardKey.KEY_WORLD_1
                GLFW.GLFW_KEY_WORLD_2 -> KeyboardKey.KEY_WORLD_2
                GLFW.GLFW_KEY_ESCAPE -> KeyboardKey.KEY_ESCAPE
                GLFW.GLFW_KEY_ENTER -> KeyboardKey.KEY_ENTER
                GLFW.GLFW_KEY_TAB -> KeyboardKey.KEY_TAB
                GLFW.GLFW_KEY_BACKSPACE -> KeyboardKey.KEY_BACKSPACE
                GLFW.GLFW_KEY_INSERT -> KeyboardKey.KEY_INSERT
                GLFW.GLFW_KEY_DELETE -> KeyboardKey.KEY_DELETE
                GLFW.GLFW_KEY_RIGHT -> KeyboardKey.KEY_RIGHT
                GLFW.GLFW_KEY_LEFT -> KeyboardKey.KEY_LEFT
                GLFW.GLFW_KEY_DOWN -> KeyboardKey.KEY_DOWN
                GLFW.GLFW_KEY_UP -> KeyboardKey.KEY_UP
                GLFW.GLFW_KEY_PAGE_UP -> KeyboardKey.KEY_PAGE_UP
                GLFW.GLFW_KEY_PAGE_DOWN -> KeyboardKey.KEY_PAGE_DOWN
                GLFW.GLFW_KEY_HOME -> KeyboardKey.KEY_HOME
                GLFW.GLFW_KEY_END -> KeyboardKey.KEY_END
                GLFW.GLFW_KEY_CAPS_LOCK -> KeyboardKey.KEY_CAPS_LOCK
                GLFW.GLFW_KEY_SCROLL_LOCK -> KeyboardKey.KEY_SCROLL_LOCK
                GLFW.GLFW_KEY_NUM_LOCK -> KeyboardKey.KEY_NUM_LOCK
                GLFW.GLFW_KEY_PRINT_SCREEN -> KeyboardKey.KEY_PRINT_SCREEN
                GLFW.GLFW_KEY_PAUSE -> KeyboardKey.KEY_PAUSE
                GLFW.GLFW_KEY_F1 -> KeyboardKey.KEY_F1
                GLFW.GLFW_KEY_F2 -> KeyboardKey.KEY_F2
                GLFW.GLFW_KEY_F3 -> KeyboardKey.KEY_F3
                GLFW.GLFW_KEY_F4 -> KeyboardKey.KEY_F4
                GLFW.GLFW_KEY_F5 -> KeyboardKey.KEY_F5
                GLFW.GLFW_KEY_F6 -> KeyboardKey.KEY_F6
                GLFW.GLFW_KEY_F7 -> KeyboardKey.KEY_F7
                GLFW.GLFW_KEY_F8 -> KeyboardKey.KEY_F8
                GLFW.GLFW_KEY_F9 -> KeyboardKey.KEY_F9
                GLFW.GLFW_KEY_F10 -> KeyboardKey.KEY_F10
                GLFW.GLFW_KEY_F11 -> KeyboardKey.KEY_F11
                GLFW.GLFW_KEY_F12 -> KeyboardKey.KEY_F12
                GLFW.GLFW_KEY_F13 -> KeyboardKey.KEY_F13
                GLFW.GLFW_KEY_F14 -> KeyboardKey.KEY_F14
                GLFW.GLFW_KEY_F15 -> KeyboardKey.KEY_F15
                GLFW.GLFW_KEY_F16 -> KeyboardKey.KEY_F16
                GLFW.GLFW_KEY_F17 -> KeyboardKey.KEY_F17
                GLFW.GLFW_KEY_F18 -> KeyboardKey.KEY_F18
                GLFW.GLFW_KEY_F19 -> KeyboardKey.KEY_F19
                GLFW.GLFW_KEY_F20 -> KeyboardKey.KEY_F20
                GLFW.GLFW_KEY_F21 -> KeyboardKey.KEY_F21
                GLFW.GLFW_KEY_F22 -> KeyboardKey.KEY_F22
                GLFW.GLFW_KEY_F23 -> KeyboardKey.KEY_F23
                GLFW.GLFW_KEY_F24 -> KeyboardKey.KEY_F24
                GLFW.GLFW_KEY_F25 -> KeyboardKey.KEY_F25
                GLFW.GLFW_KEY_KP_0 -> KeyboardKey.KEY_KP_0
                GLFW.GLFW_KEY_KP_1 -> KeyboardKey.KEY_KP_1
                GLFW.GLFW_KEY_KP_2 -> KeyboardKey.KEY_KP_2
                GLFW.GLFW_KEY_KP_3 -> KeyboardKey.KEY_KP_3
                GLFW.GLFW_KEY_KP_4 -> KeyboardKey.KEY_KP_4
                GLFW.GLFW_KEY_KP_5 -> KeyboardKey.KEY_KP_5
                GLFW.GLFW_KEY_KP_6 -> KeyboardKey.KEY_KP_6
                GLFW.GLFW_KEY_KP_7 -> KeyboardKey.KEY_KP_7
                GLFW.GLFW_KEY_KP_8 -> KeyboardKey.KEY_KP_8
                GLFW.GLFW_KEY_KP_9 -> KeyboardKey.KEY_KP_9
                GLFW.GLFW_KEY_KP_DECIMAL -> KeyboardKey.KEY_KP_DECIMAL
                GLFW.GLFW_KEY_KP_DIVIDE -> KeyboardKey.KEY_KP_DIVIDE
                GLFW.GLFW_KEY_KP_MULTIPLY -> KeyboardKey.KEY_KP_MULTIPLY
                GLFW.GLFW_KEY_KP_SUBTRACT -> KeyboardKey.KEY_KP_SUBTRACT
                GLFW.GLFW_KEY_KP_ADD -> KeyboardKey.KEY_KP_ADD
                GLFW.GLFW_KEY_KP_ENTER -> KeyboardKey.KEY_KP_ENTER
                GLFW.GLFW_KEY_KP_EQUAL -> KeyboardKey.KEY_KP_EQUAL
                GLFW.GLFW_KEY_LEFT_SHIFT -> KeyboardKey.KEY_LEFT_SHIFT
                GLFW.GLFW_KEY_LEFT_CONTROL -> KeyboardKey.KEY_LEFT_CONTROL
                GLFW.GLFW_KEY_LEFT_ALT -> KeyboardKey.KEY_LEFT_ALT
                GLFW.GLFW_KEY_LEFT_SUPER -> KeyboardKey.KEY_LEFT_SUPER
                GLFW.GLFW_KEY_RIGHT_SHIFT -> KeyboardKey.KEY_RIGHT_SHIFT
                GLFW.GLFW_KEY_RIGHT_CONTROL -> KeyboardKey.KEY_RIGHT_CONTROL
                GLFW.GLFW_KEY_RIGHT_ALT -> KeyboardKey.KEY_RIGHT_ALT
                GLFW.GLFW_KEY_RIGHT_SUPER -> KeyboardKey.KEY_RIGHT_SUPER
                GLFW.GLFW_KEY_MENU -> KeyboardKey.KEY_MENU
                GLFW.GLFW_KEY_UNKNOWN -> KeyboardKey.KEY_UNKNOWN
                else -> KeyboardKey.KEY_UNKNOWN
            }
        }
    }
}