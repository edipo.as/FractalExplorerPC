package com.fractalExplorer.platformSpecific.windows.glfw

import com.fractalExplorer.dataTypes.CoordenadasTelaI
import com.fractalExplorer.dataTypes.GLFWWindowHandle
import org.lwjgl.glfw.GLFW

fun getDesktopResolution(): CoordenadasTelaI {
    val videoMode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor())
    return CoordenadasTelaI(videoMode?.width() ?: 0, videoMode?.height() ?: 0)
}

fun getOGLWindowResolution(windowHandle: GLFWWindowHandle): CoordenadasTelaI {
    val x = IntArray(1)
    val y = IntArray(1)

    GLFW.glfwGetWindowSize(windowHandle, x, y)

    return CoordenadasTelaI(x[0], y[0])
}