package com.fractalExplorer.threads

import com.fractalExplorer.platformSpecific.windows.WindowsApplication

//todo verificar a melhor maneira de interromper
//todo
class ThreadProcessamento(private val windowsApplication: WindowsApplication) : LoopThread() {

    override val threadName = "Processamento de Iteracoes"

    override fun loop() {
        windowsApplication.janelasFractal.all { janelaFractal ->
            val possuiTarefas = janelaFractal.viewPortFractal.executaPrimeiraTarefaDeProcessamento()
            !possuiTarefas
        }.also { npossuiTarefas ->
            if (npossuiTarefas) {
                sleep(15)
           //     println("idle")
            }
        }
    }

    companion object {
        var totalThreads = 0
    }

    //todo quem vai ser responsavel pelo thread id?
    private val threadId = totalThreads

    init {
        totalThreads++
        println("Total Threads = $totalThreads")
    }

}