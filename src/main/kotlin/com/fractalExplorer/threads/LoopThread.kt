package com.fractalExplorer.threads

abstract class LoopThread : Thread() {

    companion object {
        val numeroInstancias = emptyMap<String, Int>().toMutableMap()
    }

    open val threadName: String = "A"

    abstract fun loop()

    open fun aoIniciar() {}
    open fun aoEncerrar() {}

    //todo verificar comportamento
    init {
        numeroInstancias[threadName] = numeroInstancias[threadName] ?: 0
    }

    final override fun run() {
        aoIniciar()
        try {
            while (true) {
                if (interrupted()) {
                    println("Thread $threadName Interrupted")
                    // We've been interrupted: no more crunching.
                    return
                }
                loop()
            }
        } catch (e: InterruptedException) {
            val cause = e.cause
            val causeclass: Class<Throwable>? = cause?.javaClass

            println(causeclass.toString() + "---" + e.message)
            println("Thread Processamento $threadName pegou exceção $e")
        }
        aoEncerrar()
    }
}