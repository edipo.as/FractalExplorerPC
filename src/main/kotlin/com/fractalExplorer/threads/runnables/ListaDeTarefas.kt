package com.fractalExplorer.threads.runnables

import java.util.concurrent.ConcurrentLinkedQueue

//todo verificar se posso remover esse wrapper
class ListaDeTarefas<T : Runnable> {

    private var tarefas = ConcurrentLinkedQueue<T>()

    /** é importante executar as tarefas depois de abrir a trava*/
    fun add(tarefa: T) {
        tarefas.add(tarefa)
    }

    fun executarTodas() {
        val tarefasClone = tarefas.toList()
        tarefas.clear()
        tarefasClone.forEach { it.run() }
    }

    fun getQtdeTarefas(): Int {
        return tarefas.size
    }

    fun executarPrimeira(): Boolean {
        if (tarefas.isNotEmpty()) {
            val tarefa = tarefas.remove()
            tarefa.run()
            return true
        }
        return false
    }

    fun retornarPrimeiraOrNull(): T? {
        if (tarefas.isNotEmpty()) {
            return tarefas.remove()
        }
        return null
    }
}


//TODO: implementar funcao consumetask