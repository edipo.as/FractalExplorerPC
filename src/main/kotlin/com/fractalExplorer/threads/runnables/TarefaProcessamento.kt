package com.fractalExplorer.threads.runnables

import com.fractalExplorer.viewPort.Celula

class TarefaProcessamento(private val celula: Celula) : Runnable {
    override fun run() {
        if (!celula.marcadaParaDestruicao) {
            celula.processaIteracoesECriaTextura()
            celula.matrizIteracoesEstaCalculada = true
        }
    }
}

