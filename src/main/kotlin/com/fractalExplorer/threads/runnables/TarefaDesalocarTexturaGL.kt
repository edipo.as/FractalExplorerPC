package com.fractalExplorer.threads.runnables

import com.fractalExplorer.openGl.TextureWrapper

class TarefaDesalocarTexturaGL(private val textura: TextureWrapper) : Runnable {
    override fun run() {
        textura.liberarRecursosOGL()

        ////Log.i("DesalocarTexturaGL ","Desalocou textura Gl " + textura.getHandle())
    }
}

//TODO: BUG se eu chamar tarefa desalocar antes de tarefa alocar, causa vazamento de memória