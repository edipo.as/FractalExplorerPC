package com.fractalExplorer.threads.runnables

import com.fractalExplorer.openGl.TextureWrapper

class TarefaCriarTexturaGL(private val textura: TextureWrapper) : Runnable {
    override fun run() {
        if (!textura.possuiTexturaOGLAlocada()) {
            textura.createOGLTexture()
            ////Log.i("TarefaCriarTexturaGL ","executou tarefa criar textura Gl " + textura.getHandle())
        }
    }
}

