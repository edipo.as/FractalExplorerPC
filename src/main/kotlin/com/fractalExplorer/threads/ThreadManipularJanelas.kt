package com.fractalExplorer.threads

import com.fractalExplorer.viewPort.ViewPortFractal

class ThreadManipularJanelas(val viewPortFractal: ViewPortFractal) : LoopThread() {

    override val threadName = "ManipularJanelas"

    override fun loop() {
        viewPortFractal.atualizaCamadasECelulas()
    }
}