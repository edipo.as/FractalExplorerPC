package com.fractalExplorer.paletas

import com.fractalExplorer.dataTypes.CorRGB
import com.fractalExplorer.viewPort.GerenciadorTarefasTexturas
import kotlin.math.sin

class PaletaRgbSenoidal(gerenciadorTarefasTexturas: GerenciadorTarefasTexturas) : Paleta(gerenciadorTarefasTexturas, IntArray(1024) { cor(it) })

fun cor(iteracoes: Int): Int {

    var iteracoesFloat = (iteracoes).toDouble()
    iteracoesFloat *= (2 * 3.1416 / 1024)

    val r = 0.5 + 0.5 * sin(iteracoesFloat)
    val g = 0.5 + 0.5 * sin(iteracoesFloat + 2.0)
    val b = 0.5 + 0.5 * sin(iteracoesFloat - 2.0)

    return CorRGB(r, g, b).toInt()
}