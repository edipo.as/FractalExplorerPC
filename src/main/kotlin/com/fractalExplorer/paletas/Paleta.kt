package com.fractalExplorer.paletas

import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.openGl.TextureWrapper
import com.fractalExplorer.platformSpecific.windows.GerenciadorDeImplementacoes
import com.fractalExplorer.threads.runnables.TarefaCriarTexturaGL
import com.fractalExplorer.viewPort.GerenciadorTarefasTexturas

open class Paleta(gerenciadorTarefasTexturas: GerenciadorTarefasTexturas, pixels: TArrayPixels32) {

    var tamPaleta: Int = 0
    var texturaPaleta: TextureWrapper

    init {
        tamPaleta = pixels.size
        texturaPaleta = GerenciadorDeImplementacoes.texturaPaleta(Vetor2i(tamPaleta, 1), pixels)
        gerenciadorTarefasTexturas.tarefasAlocarTextura.add(
                TarefaCriarTexturaGL(
                        texturaPaleta
                )
        )
    }

    fun bind() {
        texturaPaleta.bind()
    }
}