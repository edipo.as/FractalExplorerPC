package com.fractalExplorer.paletas

import com.fractalExplorer.dataTypes.CorRGB
import com.fractalExplorer.dataTypes.IdxPaleta
import com.fractalExplorer.dataTypes.TimeMillisecondsI
import com.fractalExplorer.viewPort.ViewPortFractal
import kotlin.math.pow
import kotlin.math.sign


//todo poderia ser companion de paleta?

//Global -> Paletas
// Por janela -> paleta atual, offset

class GerenciadorPaletas(val viewPortFractal: ViewPortFractal) {

    var totalPaletas = 0
    var idxPaletaAtual: IdxPaleta = 0

    private var paletas = emptyMap<IdxPaleta, Paleta>().toMutableMap()

    init {
       // println("GERENCIADOR DE PALETAS")
        addPaleta(PaletaRgbSenoidal(viewPortFractal.gerenciadorTarefasTexturas))
        PaletasHardCoded.paletas.forEach {
            addPaleta(Paleta(viewPortFractal.gerenciadorTarefasTexturas, it.invert()))
        }
    }

    //todo criar tipo espacial e teste unitário e ajustar sets para qualquer numero
    var offsetPaleta = 0.0 // range [0.0 1.0]
        set(value) {
            var newValue = value
            if (value < 0.0) newValue += 1.0
            if (value > 1.0) newValue -= 1.0
            field = newValue
        }

    fun atualizarPosicaoPaleta(incrementoTempo: TimeMillisecondsI) {
        if (viewPortFractal.mutableProperties.circularCores) {
            val iteracoesPorSegundo =
                viewPortFractal.mutableProperties.velocidadeCircularCores.pow(2) * sign(viewPortFractal.mutableProperties.velocidadeCircularCores)
            //   val paletasPorSegundo = iteracoesPorSegundo / janelaFractal.iteracoesPorPaleta
            val paletasPorSegundo = iteracoesPorSegundo / 256.0
            offsetPaleta += incrementoTempo.toDouble() * paletasPorSegundo
        }
    }

    fun bindCurrent() {
        paletas[idxPaletaAtual]?.bind()
        println("Bind Paleta $idxPaletaAtual total $totalPaletas")
    }

    fun addPaleta(paleta: Paleta) {
        val idxPaleta = totalPaletas
        totalPaletas++
        paletas[idxPaleta] = paleta
        idxPaletaAtual = idxPaleta
    }

    fun bindNext() {
        idxPaletaAtual++
        idxPaletaAtual %= totalPaletas
        bindCurrent()
    }

    fun bindAnterior() {
        idxPaletaAtual--
        idxPaletaAtual += totalPaletas
        idxPaletaAtual %= totalPaletas
        bindCurrent()
    }

    fun getTamPaletaAtual(): Int {
        return paletas[idxPaletaAtual]?.tamPaleta ?: 0
    }


}


fun IntArray.invert() = this.map {
    CorRGB.fromBRG(it).toInt()
}.toIntArray()

