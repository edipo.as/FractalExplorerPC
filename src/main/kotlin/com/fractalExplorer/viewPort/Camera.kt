package com.fractalExplorer.viewPort

import com.fractalExplorer.dataTypes.*
import com.fractalExplorer.viewPort.timeDepententValue.FirstOrderFunction
import kotlin.math.pow

internal class Camera(
    private val viewPortFractal: ViewPortFractal,
    cameraInicial: CameraPosition
) {

    val pos = FirstOrderFunction(cameraInicial)

    var dimensaoJanelaSaida = Vetor2d(0.0, 0.0)
        set(value) {
            field = value
            verificarLimites()
        }

    fun moverPosAtual(incremento: CoordenadasTelaF) {
        pos.atual = CameraPosition(
            pos.atual.pos + incremento * (pos.atual.delta * viewPortFractal.mutableProperties.fatorScroll),
            pos.atual.delta,
            pos.atual.angulo
        )
        pos.desejada = pos.atual
        verificarLimites()
    }

    //todo alterar arguemento
    fun moverPosDesejada(dXpixels: Float, dYpixels: Float) {

        val incremento = CoordenadasTelaF(dXpixels.toDouble(), dYpixels.toDouble())
        val fatorScrollTeclas = 0.4
        pos.desejada = CameraPosition(
            pos.desejada.pos + dimensaoJanelaSaida * incremento * (pos.atual.delta * fatorScrollTeclas),
            pos.atual.delta,
            pos.atual.angulo
        )
        verificarLimites()
    }

    fun irPara(camera: CameraPosition) {
        pos.desejada = camera
        verificarLimites()
    }

    fun dividirDeltaPor(fator: Double) {
        println("DELTA ")
        pos.atual = CameraPosition(
            pos.atual.pos,
            pos.atual.delta / fator.pow(viewPortFractal.mutableProperties.fatorEscalaDesktop),
            pos.atual.angulo
        )

        pos.desejada = pos.atual
        verificarLimites()
    }

    fun zoomPosDesejada(fator: Double) {
        pos.desejada = CameraPosition(
            pos.desejada.pos,
            pos.desejada.delta / fator.pow(viewPortFractal.mutableProperties.fatorEscalaDesktop),
            pos.desejada.angulo
        )
        verificarLimites()
    }

    fun zoomPosDesejadaPontoFixo(fator: Double, coordenadasMouse: CoordenadasTelaF) {
        val deltaDesejado = pos.desejada.delta / fator.pow(viewPortFractal.mutableProperties.fatorEscalaDesktop)
        val coordenadasTelaPontoFixo = posicaoCursorNaTela(coordenadasMouse)

        val razaoDelta = deltaDesejado / pos.atual.delta
        val posicaoAtualTelaNovaCameraDesejada = CoordenadasTelaF(
            +coordenadasTelaPontoFixo.x - coordenadasTelaPontoFixo.x * razaoDelta,
            +coordenadasTelaPontoFixo.y - coordenadasTelaPontoFixo.y * razaoDelta
        )
        pos.desejada = CameraPosition(posicaoAtualTelaNovaCameraDesejada.toCoordenadasPlano(pos.atual), deltaDesejado)
        // println("cin $coordenadasMouse cameraDesejada $cameraDesejada")
        verificarLimites()
    }

    //todo coordenadasTela Canto vs coordenadasTela Centro
    fun posicaoCursorNaTela(coordenadasMouse: CoordenadasTelaF): CoordenadasTelaF {
        return CoordenadasTelaF(
            coordenadasMouse.x - dimensaoJanelaSaida.x / 2,
            coordenadasMouse.y - dimensaoJanelaSaida.y / 2
        )
    }

    //todo limitar
    private fun verificarLimites() {
        val deltaMax = 3 / dimensaoJanelaSaida.x
        val deltaMin = 10.0.pow(-14) / dimensaoJanelaSaida.x

        val posMin = Vetor2d(-2.0, -2.0)
        val posMax = Vetor2d(2.0, 2.0)

        /*
        pos.desejada = CameraPosition(
            pos.desejada.pos.coerceIn(posMin, posMax),
            pos.desejada.delta.coerceIn(deltaMin, deltaMax),
            pos.desejada.angulo
        )*/


    }

    //todo fator em funcao do tempo
    fun advanceInTime(deltaTime: TimeMillisecondsI) {

        pos.advanceInTime(deltaTime)

/*        val logDeltaD = ln(desejada.delta)
        val logDelta = ln(atual.delta)

        atual.delta = exp(logDelta + (logDeltaD - logDelta) * 0.1)

        atual.pos += (desejada.pos - atual.pos) * 0.1

        atual.angulo += (desejada.angulo() - atual.angulo()) * 0.1*/
        //         println("delta ${cameraAtual.delta * dimensaoJanelaSaida.x} ")
    }
}