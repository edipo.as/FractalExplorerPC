package com.fractalExplorer.viewPort

import com.fractalExplorer.dataTypes.*
import com.fractalExplorer.threads.runnables.ListaDeTarefas
import com.fractalExplorer.threads.runnables.TarefaProcessamento


/**
Os vetores coordCelulasX(CX) e Os coordCelulasY(CY) armazendam as coordenadas das colunas e das linhas

As coordenadas no plano são copiadas paras as células por valor, visto que são constantes.


-------CX0 CX1 CX2 CX3

CY0    c00 c10 c20 c30
CY1    c01 c11 c21 c31
CY2    c02 c12 c22 c32
CX3    c03 c13 c23 c33

A matriz de células é implementada usando uma Lista (colunas) de lista (linhas) de células

Esta classe possui métodos para inserir e remover linhas e colunas,
estes métodos manipulam e os vetores coordCelulas e a matriz de células ao mesmo tempo
 */


class Camada(val viewPortFractal: ViewPortFractal, coordenadasIniciais: CameraPosition) {
    enum class PosicaoColuna {
        PRIMEIRA, ULTIMA
    }

    /**lista de tarefas*/
    val tarefasProcessamento =
            ListaDeTarefas<TarefaProcessamento>()

    /**Vetor de Coordenadas das células*/
    private var coordCelulasX = MutableList<CoordenadaPlano>(1) {
        coordenadasIniciais.pos.x
    }
    private var coordCelulasY = MutableList<CoordenadaPlano>(1) {
        coordenadasIniciais.pos.y
    }

    internal val delta: TDelta = coordenadasIniciais.delta

    /**Lista (colunas) de lista (linhas) de células*/
    internal var celulas = MutableList<MutableList<Celula>>(1) {
        MutableList(1) {
            Celula(this, coordenadasIniciais.pos, viewPortFractal.immutableProperties.tamSprite)
        }
    }

    fun atualizaMatrizDeCelulas() {
        //TODO:usar tipos e operator overload para melhorar
        val tamSpriteNoPlano = CoordenadasPlano(
                viewPortFractal.immutableProperties.tamSprite.x.toDouble() * delta,
                viewPortFractal.immutableProperties.tamSprite.y.toDouble() * delta
        )

        val retanguloAlocar = RetanguloPlano(viewPortFractal.regiaoDesenhoPlano).apply {
            max -= tamSpriteNoPlano
        }

        val retanguloDesalocar = RetanguloPlano(viewPortFractal.regiaoDesenhoPlano).apply {
            min -= tamSpriteNoPlano
        }

        while (coordCelulasX.first() > retanguloAlocar.min.x) adicionarColuna(PosicaoColuna.ULTIMA)
        while (coordCelulasX.last() < retanguloAlocar.max.x) adicionarColuna(PosicaoColuna.PRIMEIRA)
        while (coordCelulasY.first() > retanguloAlocar.min.y) adicionarLinha(PosicaoColuna.ULTIMA)
        while (coordCelulasY.last() < retanguloAlocar.max.y) adicionarLinha(PosicaoColuna.PRIMEIRA)

        if (coordCelulasX.size > 1) {
            if (coordCelulasX.first() < retanguloDesalocar.min.x) removerColuna(PosicaoColuna.ULTIMA)
            if (coordCelulasX.last() > retanguloDesalocar.max.x) removerColuna(PosicaoColuna.PRIMEIRA)
        }
        if (coordCelulasY.size > 1) {
            if (coordCelulasY.first() < retanguloDesalocar.min.y) removerLinha(PosicaoColuna.ULTIMA)
            if (coordCelulasY.last() > retanguloDesalocar.max.y) removerLinha(PosicaoColuna.PRIMEIRA)
        }
        //    println("qdte celulas x: ${coordCelulasX.size} y: ${coordCelulasY.size}")
    }

    fun getCoordenadasPlanoCantoSupEsq(): CoordenadasPlano {
        return CoordenadasPlano(
                coordCelulasX.first(),
                coordCelulasY.first()
        )
    }

    //TODO: Adicionar Coluna e linha não seta coordenadas tela dos vetores, eles ficam em zero durante um frame
    //TODO: refatorar nomes
    private fun adicionarColuna(posicao: PosicaoColuna) {
        val tamX = coordCelulasX.size

        val indiceX = if (posicao == PosicaoColuna.PRIMEIRA) (tamX) else (0)

        val offset =
                if (posicao == PosicaoColuna.PRIMEIRA) (tamX * delta * viewPortFractal.immutableProperties.tamSprite.x) else (-delta * viewPortFractal.immutableProperties.tamSprite.x)

        /** incrementa o vetor de coordenadas */
        val novaCoordenadaX = coordCelulasX.first() + offset
        coordCelulasX.add(indiceX, novaCoordenadaX)

        /**cria uma nova coluna */
        val novaColuna = emptyList<Celula>().toMutableList()

        /**Popula a coluna criada com uma linha para cada coordenada Y*/
        coordCelulasY.forEachIndexed { indiceY, it ->
            val novaCelula = Celula(
                    this, CoordenadasPlano(
                    coordCelulasX[indiceX],
                    coordCelulasY[indiceY]
            ),
                    viewPortFractal.immutableProperties.tamSprite
            )
            novaColuna.add(novaCelula)
        }

        /**Insere nova Coluna na matriz de células */
        celulas.add(indiceX, novaColuna)
    }


    private fun adicionarLinha(posicao: PosicaoColuna) {
        val tamY = coordCelulasY.size

        val indiceY = if (posicao == PosicaoColuna.PRIMEIRA) (tamY) else (0)

        val novacoordenadaOffset =
                if (posicao == PosicaoColuna.PRIMEIRA) (tamY * delta * viewPortFractal.immutableProperties.tamSprite.y) else (-delta * viewPortFractal.immutableProperties.tamSprite.y)

        /** incrementa o vetor de coordenadas */
        val novaCoordenadaY = coordCelulasY.first() + novacoordenadaOffset
        coordCelulasY.add(indiceY, novaCoordenadaY)

        /** para cada coluna ...*/
        coordCelulasX.forEachIndexed { indiceX, it ->
            /** ... cria uma nova célula*/
            val novaCelula = Celula(
                    this,
                    CoordenadasPlano(
                            coordCelulasX[indiceX],
                            coordCelulasY[indiceY]
                    ),
                    viewPortFractal.immutableProperties.tamSprite
            )
            celulas[indiceX].add(indiceY, novaCelula)
        }
    }


    private fun removerColuna(posicao: PosicaoColuna) {
        val tamX = coordCelulasX.size

        val indiceX = if (posicao == PosicaoColuna.PRIMEIRA) (tamX - 1) else (0)

        /** remove Coordenadas do vetor*/
        coordCelulasX.removeAt(indiceX)

        /** liberar recursos das células*/
        celulas[indiceX].forEach { celula ->
            celula.liberaRecursos()
        }

        /** remove coluna*/
        celulas.removeAt(indiceX)
    }

    private fun removerLinha(posicao: PosicaoColuna) {
        val tamY = coordCelulasY.size

        val indiceY = if (posicao == PosicaoColuna.PRIMEIRA) (tamY - 1) else (0)

        /** remove Coordenadas do vetor*/
        coordCelulasY.removeAt(indiceY)

        /** liberar recursos das células*/
        celulas.forEach { linha ->
            linha[indiceY].liberaRecursos()
        }

        /** remove linha*/
        celulas.forEach { linha ->
            linha.removeAt(indiceY)
        }
    }

    fun liberarRecursos() {
        celulas.forEach { linha ->
            linha.forEach { celula ->
                celula.liberaRecursos()
            }
        }
    }
}