package com.fractalExplorer.viewPort

import com.fractalExplorer.dataTypes.CoordenadasTelaI
import com.fractalExplorer.dataTypes.Vetor2i

const val ladoSpritePadrao: Int = 128

data class ImmutableProperties(
    val tamSprite: Vetor2i = Vetor2i(ladoSpritePadrao, ladoSpritePadrao),
    val tamanhoInicialTela: CoordenadasTelaI = CoordenadasTelaI(2500, 1500)
)
