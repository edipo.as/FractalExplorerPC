package com.fractalExplorer.viewPort


import com.fractalExplorer.dataTypes.CoordenadasPlano
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.openGl.Sprite
import com.fractalExplorer.platformSpecific.windows.GerenciadorDeImplementacoes
import com.fractalExplorer.threads.runnables.TarefaDesalocarTexturaGL
import com.fractalExplorer.threads.runnables.TarefaProcessamento

class Celula(val camada: Camada, private val coordenadasPlano: CoordenadasPlano, val tamTextura: Vetor2i) :
        Sprite( camada.viewPortFractal.texturaPlaceholder) {

    var matrizIteracoesEstaCalculada = false

    var iteracoes = camada.viewPortFractal.poolArrayIteracoes.borrowObject()

    init {
        camada.tarefasProcessamento.add(
                TarefaProcessamento(this)
        )
    }

    fun processaIteracoesECriaTextura() {
        processaIteracoes()
        textura = GerenciadorDeImplementacoes.texturaCelula(tamTextura, iteracoes)
        solicitarGeracaoDeTexturaGL(camada.viewPortFractal.gerenciadorTarefasTexturas.tarefasAlocarTextura)
    }


    private fun processaIteracoes() {
        with(camada.viewPortFractal) {
            val fractal = camada.viewPortFractal.fractal
            val delta = camada.delta
            for (j in 0 until tamTextura.y) {
                for (i in 0 until tamTextura.x) {
                    val pixelIndex = i + j * tamTextura.x
                    iteracoes[pixelIndex] = if (pixelTransparente(i, j)) {
                        com.fractalExplorer.openGl.pixelTransparente
                    } else {
                        fractal.iteracoesNormalizadaI(
                                coordenadasPlano.x + i * delta,
                                coordenadasPlano.y + j * delta,
                            mutableProperties.limiteDivergencia,
                            mutableProperties.maxIteracoes,
                            mutableProperties.samplesPorIteracao
                        )
                    }
                }
            }
        }
    }

    private fun pixelTransparente(x: Int, y: Int) = false // ((x % 2 == 1) && (y % 2 == 1))

    fun liberaRecursos() {
        marcadaParaDestruicao = true
        camada.viewPortFractal.apply {
            //poolTextureWrapper.returnObject(textura)

            gerenciadorTarefasTexturas.tarefasDesalocarTextura.add(
                    TarefaDesalocarTexturaGL(textura)
            )
            poolArrayIteracoes.returnObject(iteracoes)
        }
    }

}