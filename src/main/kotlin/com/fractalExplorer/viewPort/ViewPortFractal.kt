package com.fractalExplorer.viewPort

import com.fractalExplorer.ObjectPool
import com.fractalExplorer.dataTypes.*
import com.fractalExplorer.dummyTexture
import com.fractalExplorer.fractal.Fractal
import com.fractalExplorer.openGl.glProgramSpriteArrayOpenGL
import com.fractalExplorer.platformSpecific.windows.GerenciadorDeImplementacoes
import com.fractalExplorer.threads.ThreadManipularJanelas
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.math.log
import kotlin.math.pow

//TODO: thread Safe
/** Lock janela thread
 * locks nas filas de tarefas
 * tarefa deve conter todas as informacoes para realizar
 * tarefas sao executadas por outras trheads
 * lock dentro da célula nos buffers de entrada e resultado do processamento, textura???
 * lock nas camadas caso queira criar thread exclusiva de adicionar/remover ,
 * somente caso esteja muito lento (profiling) seria a ultima coisa a fazer
 */
class ViewPortFractal(
        val gerenciadorTarefasTexturas: GerenciadorTarefasTexturas,
        val fractal: Fractal
) {

    val immutableProperties = ImmutableProperties()
    val mutableProperties = MutableProperties()

    var offsetDebug = 0
        set(value) {
            field = value
            println(value)
        }

    internal val camera = Camera(this, mutableProperties.cameraInicial)

    private val lock = ReentrantLock()

    var dummyTexture = TArrayIteracoes(1024) { it -> dummyTexture(it.toFloat(), 1.0f).toInt() }

    val texturaPlaceholder = GerenciadorDeImplementacoes.texturaCelula(
            Vetor2i(100, 100),
            dummyTexture
    )

    var regiaoDesenhoPlano = RetanguloPlano()


    val poolArrayIteracoes = ObjectPool(1000, 10000, 1L,
            { TArrayIteracoes(immutableProperties.tamSprite.x * immutableProperties.tamSprite.y) { 0 } }
    )

    private val threadManipularJanelas = ThreadManipularJanelas(this)

    var camadas: SortedMap<FatorAmpliacaoI, Camada> = emptyMap<FatorAmpliacaoI, Camada>().toSortedMap()

    //todo deve ser propriedade da janela fractal
    private val textoDebug = StringBuilder()

    init {
        atualizaCamadasECelulas()
        threadManipularJanelas.start()
    }

    private fun atualizarTexto() {
        textoDebug.clear()
        var coordenadasTela = camera.dimensaoJanelaSaida
        coordenadasTela -= camera.dimensaoJanelaSaida / 2.0


        textoDebug.append("\nTouch $coordenadasTela")
        textoDebug.append("\nPlano ${coordenadasTela.toCoordenadasPlano(camera.pos.atual)}")
        textoDebug.append("\n $this")

    }

    fun atualizaCamadasECelulas() {
        lock.lock()
        adicionarRemoverCamadas()
        atualizaIntervaloDesenho()
        camadas.values.forEach { camada ->
            lock.unlock()
            lock.lock()
            camada.atualizaMatrizDeCelulas()
        }
        lock.unlock()
    }

    //todo unfiicar responsabilidade de fornecer instante atual
    fun desenharFrame(
            glProgramSprites: glProgramSpriteArrayOpenGL
    ) {
        lock.lock()
        if (mutableProperties.ExibirInformacoesDaJanelaEmOverlay) atualizarTexto()


        camera.advanceInTime(15)

        camadas.values.forEach { camada ->
            val posicaoCantoSupEsquerdoCamadaTela =
                camada.getCoordenadasPlanoCantoSupEsq().toCoordenadasTela(camera.pos.atual)
            val escala = immutableProperties.tamSprite.toVetor2d() * (camada.delta / camera.pos.atual.delta)
            camada.celulas.forEachIndexed { i, coluna ->
                coluna.forEachIndexed { j, celula ->
                    val indiceRelativoNaCamada = Vetor2i(i, j)
                    if (celula.possuiTexturaValida() && celula.matrizIteracoesEstaCalculada) {
                        glProgramSprites.desenharSpriteArray(
                            celula,
                            escala,
                            camera.pos.atual.angulo,
                            indiceRelativoNaCamada,
                            posicaoCantoSupEsquerdoCamadaTela
                        )
                    }
                }
            }
        }
        lock.unlock()
    }

    fun executaPrimeiraTarefaDeProcessamento(): Boolean {
        lock.lock()
        camadas.values.forEach { camada ->
            camada.tarefasProcessamento.retornarPrimeiraOrNull()?.let {
                lock.unlock()
                it.run()
                //println("Executando tarefa $fractal")
                return true //executou tarefa
            }
        }
        lock.unlock()
        return false //camada não possui tarefas a executar
    }

    private fun atualizaIntervaloDesenho() {
        //todo transformar janela desenho em um retangulo
        var janelaDesenho = CoordenadasTelaF(
                camera.dimensaoJanelaSaida.x * 0.5 * mutableProperties.fatorDebugJanelaDesenho,
                camera.dimensaoJanelaSaida.y * 0.5 * mutableProperties.fatorDebugJanelaDesenho
        )

        regiaoDesenhoPlano.max = janelaDesenho.toCoordenadasPlano(camera.pos.atual)
        janelaDesenho = -janelaDesenho
        regiaoDesenhoPlano.min = janelaDesenho.toCoordenadasPlano(camera.pos.atual)

    }

    private fun adicionarRemoverCamadas() {
        val maiorvalor = log((1.0 / (camera.pos.atual.delta * mutableProperties.minTamanhoAparentePixel)), 2.0).toInt()
        val menorvalor = maiorvalor - mutableProperties.quantidadeDeCamadasAlemDaPrincipal

        val camadasDesejadas = IntRange(
                menorvalor,
                maiorvalor
        )

        /** Todas as novas camadas são alinahdas a maior camada*/
        val coordenadasNovasCamadas =
                camadas.values.firstOrNull()?.getCoordenadasPlanoCantoSupEsq()
                        ?: CoordenadasPlano(-0.0, -0.0)

        //TODO:Substituir por comparação direta com o range
        camadasDesejadas.forEach {
            //TODO: usar metodo .in
            if (!camadas.containsKey(it)) {
                val coordenadas = CameraPosition(
                        coordenadasNovasCamadas,
                        getDeltaFromIntegerMagnification(it)
                )
                // 1.0/512.0 )
                println("adicionou camada $it")
                camadas.put(it, Camada(this, coordenadas))
            }//TODO:consertar delta
        }

        /** cria uma lista de todas as camadas que devem ser apagadas*/
        val camadasApagar = emptyList<Int>().toMutableList()

        camadas.forEach { (key, camada) ->
            if (key !in camadasDesejadas) {
                camadasApagar.add(key)
                camada.liberarRecursos()
            }
        }
        camadasApagar.forEach {
            camadas.remove(it)
        }
    }

    override fun toString(): String {
        lock.lock()
        val stringB = StringBuilder()
        stringB.append("Dimensoes da Janela: $camera.dimensaoJanelaSaida")
        stringB.append("\nFila Criar Textura ${gerenciadorTarefasTexturas.tarefasAlocarTextura.getQtdeTarefas()}")
        stringB.append("\nFila Desalocar Textura " + gerenciadorTarefasTexturas.tarefasDesalocarTextura.getQtdeTarefas())
        //  textoDebug.append("\nmin:" +coord_min)
        //  textoDebug.append("\nmax:" +coord_max)
        stringB.append("\nCoord Camera Atual " + camera.pos.atual)
        camadas.forEach { camada ->
            stringB.append("\n\t" + camada.key + ": Fila Processos " + camada.value.tarefasProcessamento.getQtdeTarefas())
        }
        lock.unlock()
        return stringB.toString()
    }

    private fun getDeltaFromIntegerMagnification(mag: Int): TDelta {
        return 0.5.pow(mag.toDouble())
    }

    fun liberarRecursos() {
        threadManipularJanelas.interrupt()
        poolArrayIteracoes.shutdown()

        // TODO("Not yet implemented")
    }


}