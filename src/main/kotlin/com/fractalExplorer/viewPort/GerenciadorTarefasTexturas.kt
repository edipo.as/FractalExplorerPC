package com.fractalExplorer.viewPort

import com.fractalExplorer.dataTypes.TimeMillisecondsI
import com.fractalExplorer.platformSpecific.windows.Relogio
import com.fractalExplorer.threads.runnables.ListaDeTarefas
import com.fractalExplorer.threads.runnables.TarefaCriarTexturaGL
import com.fractalExplorer.threads.runnables.TarefaDesalocarTexturaGL

class GerenciadorTarefasTexturas(private val relogio: Relogio) {

    val tarefasAlocarTextura =
            ListaDeTarefas<TarefaCriarTexturaGL>()
    val tarefasDesalocarTextura =
            ListaDeTarefas<TarefaDesalocarTexturaGL>()
    //todo classe cronometro?

    fun executaTarefasTexturas(duracaoEsperada: TimeMillisecondsI) {
        val instanteInicio = relogio.getCurrentTimeMs()
        while (relogio.getCurrentTimeMs() - instanteInicio < duracaoEsperada) {
            tarefasDesalocarTextura.executarPrimeira()
            tarefasAlocarTextura.executarPrimeira()
        }
    }

    /*private val numThreadsProcessamento: Int = Runtime.getRuntime().availableProcessors()
    private val threadsProcessamento = List(numThreadsProcessamento) { ThreadProcessamento(this) }
    private val threadManipularJanelas = ThreadManipularJanelas(this)

    fun start(){
        threadManipularJanelas.start()
        threadsProcessamento.forEach { it.start() }
    }*/
}
