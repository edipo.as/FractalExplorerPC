package com.fractalExplorer.viewPort

import com.fractalExplorer.dataTypes.CameraPosition
import com.fractalExplorer.dataTypes.CoordenadasPlano

class MutableProperties {
    val minTamanhoAparentePixel =
        1f//2f // mínimo tamanho que o pixel pode assumir na tela, valores <1 representam multisampling
    val quantidadeDeCamadasAlemDaPrincipal =
        5  // camadas de preview (baixa resolucao) processadas antes da camada de alta qualidade

    var ExibirInformacoesDaJanelaEmOverlay = false

    val samplesPorIteracao = 64
    var iteracoesPorPaleta = 256.0
        set(value) {
            field = value.coerceIn(1.0, 100000.0)
        }

    val limiteDivergencia = 16
    val maxIteracoes = 5000

    var fatorScroll = 2.0
    var fatorEscalaDesktop = 1.4

    var reverterNormalizacao = false

    //samplingIteracoes*maxIteracoes < 2^16 OU SEJA TÁ INDO 16 BIT CORRETAMENTE
    var circularCores = true
    var velocidadeCircularCores = 0.01

    //TODO: remover fator debug e criar janelas de alocação e inatividade
    //var debug = false
    var fatorDebugJanelaDesenho = 1.0//1//0.5 //tamanho da janela de alocação, usado para debug, release deve ser =1


    var TaxaZoom = 0.0f
    var TaxaPan = 0.0f
    var MagInicial = 9

    var MaginiciallF = Math.pow(0.5, MagInicial.toDouble()) / 1.12
    var cameraInicial = CameraPosition(
            CoordenadasPlano(
                    -0.75,
                    0.0
            ), MaginiciallF
    )
}