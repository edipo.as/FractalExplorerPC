package com.fractalExplorer.viewPort.timeDepententValue.camera

import com.fractalExplorer.dataTypes.CameraPosition
import com.fractalExplorer.dataTypes.TimeMilliseconds
import com.fractalExplorer.dataTypes.TimeMillisecondsI
import com.fractalExplorer.dataTypes.format
import com.fractalExplorer.viewPort.timeDepententValue.TimeDependentValue
import kotlin.math.log2
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sqrt

//TODO DOES NOT WORK
class CameraLinearP(
    var initialValue: CameraPosition,
    var timeConstant: TimeMilliseconds = 100.0
) : TimeDependentValue<CameraPosition> {

    val velocityBase = 3.0

    val tan = 1.0

    val fatorDelta = 1.0

    override var desejada: CameraPosition = initialValue
        set(value) {
            initialValue = atual
            field = value

            error = desejada.pos - atual.pos

            distance = error.abs()

            distanceTraveled = 0.0
            unitaryVector = if (distance > 0) {
                error / distance
            } else {
                error
            }

            y2 = log2(desejada.delta) - log2(atual.delta)

            if (y2 <= 0.0) {
                a = (y2 - tan * distance) / (distance * distance)
                b = tan
            } else {
                a = (-y2 - tan * distance) / (distance * distance)
                b = -tan - 2.0 * a * distance
            }
            println("y2 ${y2.format(3)} a ${a.format(3)} b ${b.format(3)}")
        }

    override var atual: CameraPosition = initialValue

    var error = desejada.pos - atual.pos
    var distance = error.abs()
    var distanceTraveled = 0.0
    var unitaryVector = if (distance > 0) {
        error / distance
    } else {
        error
    }

    var a = 0.0
    var b = 0.0
    var y2 = log2(desejada.delta) - log2(atual.delta)


    override fun advanceInTime(deltaTime: TimeMillisecondsI) {
        val inclinacao = (2 * a * distanceTraveled + b) * fatorDelta
        val modulo = sqrt(1.0 + inclinacao * inclinacao)

        val velocity = velocityBase / modulo //** atual.delta

//        println("velocity $velocity")

        distanceTraveled += velocity * (deltaTime.toDouble() / 1000.0)
        // println("distanceTraveled $distanceTraveled")

        distanceTraveled = min(distance, distanceTraveled)

        val log2delta = (a * distanceTraveled * distanceTraveled + b * distanceTraveled) * fatorDelta
        // println("Modulo ${modulo.format(3)} log2delta = $log2delta")


        val delta = 2.0.pow(+log2delta + log2(initialValue.delta))
        atual = CameraPosition(
            initialValue.pos + unitaryVector * distanceTraveled,
            delta,
            desejada.angulo
        )
        atual = atual * 1.0

        // atual = desejada - (desejada - atual) * exp(-deltaTime.toDouble() / timeConstant)
    }
}