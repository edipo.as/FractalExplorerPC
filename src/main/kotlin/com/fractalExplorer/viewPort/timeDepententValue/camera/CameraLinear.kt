package com.fractalExplorer.viewPort.timeDepententValue.camera

import com.fractalExplorer.dataTypes.CameraPosition
import com.fractalExplorer.dataTypes.TimeMilliseconds
import com.fractalExplorer.dataTypes.TimeMillisecondsI
import com.fractalExplorer.dataTypes.format
import com.fractalExplorer.viewPort.timeDepententValue.TimeDependentValue
import kotlin.math.min

//TODO DOES NOT WORK
class CameraLinear(
    var initialValue: CameraPosition,
    var timeConstant: TimeMilliseconds = 100.0
) : TimeDependentValue<CameraPosition> {

    val velocityBase = 1000.0

    override var desejada: CameraPosition = initialValue
        set(value) {
            initialValue = atual
            field = value

            error = desejada.pos - atual.pos

            distance = error.abs()

            distanceTraveled = 0.0
            unitaryVector = if (distance > 0) {
                error / distance
            } else {
                error
            }


            println("error $error")
            println("distance ${distance.format(3)}")
        }

    override var atual: CameraPosition = initialValue

    var error = desejada.pos - atual.pos
    var distance = error.abs()
    var distanceTraveled = 0.0
    var unitaryVector = if (distance > 0) {
        error / distance
    } else {
        error
    }


    override fun advanceInTime(deltaTime: TimeMillisecondsI) {
        val velocity = velocityBase * atual.delta
//        println("velocity $velocity")

        distanceTraveled += velocity * (deltaTime.toDouble() / 1000.0)
        // println("distanceTraveled $distanceTraveled")


        distanceTraveled = min(distance, distanceTraveled)

        atual = CameraPosition(
            initialValue.pos + unitaryVector * distanceTraveled,
            desejada.delta,
            desejada.angulo
        )
        atual = atual * 1.0

        // atual = desejada - (desejada - atual) * exp(-deltaTime.toDouble() / timeConstant)
    }
}