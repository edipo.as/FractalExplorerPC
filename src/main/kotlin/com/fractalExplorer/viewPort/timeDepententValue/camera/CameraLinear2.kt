package com.fractalExplorer.viewPort.timeDepententValue.camera

import com.fractalExplorer.dataTypes.CameraPosition
import com.fractalExplorer.dataTypes.TimeMilliseconds
import com.fractalExplorer.dataTypes.TimeMillisecondsI
import com.fractalExplorer.viewPort.timeDepententValue.TimeDependentValue
import kotlin.math.exp

//TODO DOES NOT WORK
class CameraLinear2(
    var initialValue: CameraPosition,
    var timeConstant: TimeMilliseconds = 100.0
) : TimeDependentValue<CameraPosition> {

    private var posDesejada = CameraLinear(initialValue, timeConstant)

    override var desejada: CameraPosition = initialValue
        get() = posDesejada.desejada
        set(value) {
            field = value
            posDesejada.desejada = value
        }

    override var atual: CameraPosition = initialValue

    override fun advanceInTime(deltaTime: TimeMillisecondsI) {
        posDesejada.advanceInTime(deltaTime)
        atual = posDesejada.atual - (posDesejada.atual - atual) * exp(-deltaTime.toDouble() / timeConstant)
    }
}