package com.fractalExplorer.viewPort.timeDepententValue

import com.fractalExplorer.dataTypes.Summable
import com.fractalExplorer.dataTypes.TimeMilliseconds
import com.fractalExplorer.dataTypes.TimeMillisecondsI
import kotlin.math.exp

//todo does not work
class SecondOrderCriticallyDamped<T : Summable<T>>(
    private var initialValue: T,
    val timeConstant: TimeMilliseconds = 100.0
) : TimeDependentValue<T> {

    val omegaN = 1e-2

    private var elapsedTime: Double = 0.0
    override var desejada: T = initialValue
        set(value) {
            field = value
            elapsedTime = 0.0
            initialValue = atual
            erro = desejada - atual
        }

    override var atual: T = initialValue
    private var erro = desejada - atual

    override fun advanceInTime(deltaTime: TimeMillisecondsI) {
        elapsedTime += deltaTime
        val expoent = -omegaN * elapsedTime
        atual = initialValue + erro + erro * (-1 + expoent) * exp(expoent)
    }
}