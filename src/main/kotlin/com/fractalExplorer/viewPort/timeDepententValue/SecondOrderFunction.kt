package com.fractalExplorer.viewPort.timeDepententValue

import com.fractalExplorer.dataTypes.Summable
import com.fractalExplorer.dataTypes.TimeMilliseconds
import com.fractalExplorer.dataTypes.TimeMillisecondsI
import kotlin.math.exp

/** first order function tracking another first order function */
class SecondOrderFunction<T : Summable<T>>(
    initialValue: T,
    var timeConstant: TimeMilliseconds = 100.0
) : TimeDependentValue<T> {

    private var posDesejada = FirstOrderFunction<T>(initialValue, timeConstant)

    override var desejada: T = initialValue
        get() = posDesejada.desejada
        set(value) {
            field = value
            posDesejada.desejada = value
        }

    override var atual: T = initialValue

    override fun advanceInTime(deltaTime: TimeMillisecondsI) {
        posDesejada.advanceInTime(deltaTime)
        atual = posDesejada.atual - (posDesejada.atual - atual) * exp(-deltaTime.toDouble() / timeConstant)
    }
}