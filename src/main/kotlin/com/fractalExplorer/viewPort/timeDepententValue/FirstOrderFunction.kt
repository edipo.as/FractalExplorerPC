package com.fractalExplorer.viewPort.timeDepententValue

import com.fractalExplorer.dataTypes.Summable
import com.fractalExplorer.dataTypes.TimeMilliseconds
import com.fractalExplorer.dataTypes.TimeMillisecondsI
import kotlin.math.exp

class FirstOrderFunction<T : Summable<T>>(
    initialValue: T,
    var timeConstant: TimeMilliseconds = 100.0
) : TimeDependentValue<T> {

    override var desejada: T = initialValue
    override var atual: T = initialValue

    override fun advanceInTime(deltaTime: TimeMillisecondsI) {
        atual = desejada - (desejada - atual) * exp(-deltaTime.toDouble() / timeConstant)
    }
}