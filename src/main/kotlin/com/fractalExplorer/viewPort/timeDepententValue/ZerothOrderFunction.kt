package com.fractalExplorer.viewPort.timeDepententValue

import com.fractalExplorer.dataTypes.Summable
import com.fractalExplorer.dataTypes.TimeMilliseconds
import com.fractalExplorer.dataTypes.TimeMillisecondsI

class ZerothOrderFunction<T : Summable<T>>(
    initialValue: T,
    var timeConstant: TimeMilliseconds = 100.0
) : TimeDependentValue<T> {

    override var desejada: T = initialValue
    override var atual: T = initialValue

    override fun advanceInTime(deltaTime: TimeMillisecondsI) {
        atual = (desejada) * 1.0
    }
}