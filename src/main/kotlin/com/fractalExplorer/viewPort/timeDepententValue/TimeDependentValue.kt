package com.fractalExplorer.viewPort.timeDepententValue

import com.fractalExplorer.dataTypes.Summable
import com.fractalExplorer.dataTypes.TimeMillisecondsI

interface TimeDependentValue<T: Summable<T>>{

    var atual: T
    var desejada: T

    fun advanceInTime(deltaTime: TimeMillisecondsI)
}