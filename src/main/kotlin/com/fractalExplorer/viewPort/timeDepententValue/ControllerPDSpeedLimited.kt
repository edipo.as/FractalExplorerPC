package com.fractalExplorer.viewPort.timeDepententValue

import com.fractalExplorer.dataTypes.FrequencyRads
import com.fractalExplorer.dataTypes.Summable
import com.fractalExplorer.dataTypes.TimeMillisecondsI
import kotlin.math.pow

class ControllerPDSpeedLimited<T : Summable<T>>(
    initialValue: T,
    var maxSpeed: T,
    var omegaN: FrequencyRads = 10.0
) : TimeDependentValue<T> {

    override var desejada: T = initialValue
    override var atual: T = initialValue

    private var speed = initialValue - initialValue

    private val freqRadPerMilliseconds = omegaN

    private val kP = freqRadPerMilliseconds.pow(2)
    private val kD = -2.0 * freqRadPerMilliseconds


    override fun advanceInTime(deltaTime: TimeMillisecondsI) {
        val time = deltaTime.toDouble() / 1000.0
        val erro = desejada - atual

        speed += (erro * kP + speed * kD) * time

        speed = speed.coerceIn(-maxSpeed, maxSpeed)

        atual += speed * time

    }
}