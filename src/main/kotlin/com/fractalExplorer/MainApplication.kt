package com.fractalExplorer

import com.fractalExplorer.platformSpecific.windows.WindowsApplication
import com.fractalExplorer.platformSpecific.windows.janelas.JanelaPrincipalFractal


//todo threads por janelas ou global??
//todo demais tarefas  e recursos por janelas ou global??

fun main() {
    //val gerenciadorPaletas = GerenciadorDePaletas()

    WindowsApplication().run {

        addJanela { JanelaPrincipalFractal() }

        startLoop()
    }
    println("program finished")
}
