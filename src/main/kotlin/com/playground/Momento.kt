package com.playground

import com.playground.tiposEspeciais.Comprimento
import com.playground.tiposEspeciais.Forca
import com.playground.tiposEspeciais.times

fun main() {
    val forca = Forca(15.0)
    val comprimento = Comprimento(1.5)

    val resultado = forca * comprimento

    println("O resultado é $resultado do tipo ${resultado::class.simpleName}")
}