package com.playground

open class Superclass {
    protected open fun onExecuteMyMethod() {}

    fun myMethod() {
        onExecuteMyMethod()
        println("Superclass does it's stuff")
    }
}

class SubClass : Superclass() {
    override fun onExecuteMyMethod() {
        println("Subclass does it's stuff")
    }
}

fun main() {
    SubClass().myMethod()
}