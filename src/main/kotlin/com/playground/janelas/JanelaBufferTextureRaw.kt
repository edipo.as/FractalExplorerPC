package com.playground.janelas

//todo convert GLFW to my own enums
import com.fractalExplorer.dataTypes.CorRGB
import com.fractalExplorer.dataTypes.ITexturaGL
import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.fractal.Fractal
import com.fractalExplorer.fractal.MandelbrotOtimizado
import com.fractalExplorer.paletas.cor
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramRectangleVertex
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramSpriteBufferTexture
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform1i
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform2f
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform2i
import com.myGraphicLibrary.glfw.GenericGlfwWindow
import org.lwjgl.opengl.GL32


//todo openGl falha se a janela perde foco

@ExperimentalUnsignedTypes
class JanelaBufferTextureRaw(val fractal: Fractal = MandelbrotOtimizado()) : GenericGlfwWindow() {

    private val glProgram = GlProgramSpriteBufferTexture("sprite/Fragment.glsl")


    private val tamTextura = Vetor2i(1024, 1024)
    private val fatorZoom = tamTextura.x / 4.0
    val pixels = TArrayPixels32(tamTextura.x * tamTextura.y) { 0 }

    private val uniformTextura = Uniform1i("u_Textura", glProgram.programHandle)
    private val uniformWindowSize = Uniform2f("u_ViewportSize", glProgram.programHandle)
    private val uniformTextureSize = Uniform2i("u_TextureSize", glProgram.programHandle)

    private val textureHandle = IntArray(1) { 0 }
    private val bufferHandle = IntArray(1) { 0 }
    private val texturaGl: ITexturaGL = GL32.GL_TEXTURE4
    private val idxUniformTextura: Int = 4


    private val pixelsIntArray = pixelArray()

    init {
        glProgram.useProgram()
        println("GL PROGRAM handle ${glProgram.programHandle}!")

        genGlTexture()
        /** TEXTURA*/

        uniformTextura.setValue(idxUniformTextura)
        uniformWindowSize.setValue(dimensoesJanela.x.toFloat(), dimensoesJanela.y.toFloat())
        uniformTextureSize.setValue(tamTextura.x, tamTextura.y)

        aCadaQuadroGlfwLambda = {

            // glProgram.glProgram.useProgram()
            // GL32.glActiveTexture(texturaGl)

            desenharSprite()

            val pidex = (mouse.x + mouse.y * tamTextura.x)
            alterarTitulo("Posicao: ${mouse.x} ${mouse.y} , pixel index: $pidex ")
        }
    }

    private fun genGlTexture() {
        println("START TEXTURE ")
        val target = GL32.GL_TEXTURE_BUFFER
        val internalFormat = GL32.GL_RGBA8

        GL32.glGenBuffers(bufferHandle)
        if (bufferHandle[0] != 0) {
            GL32.glActiveTexture(texturaGl)
            GL32.glBindBuffer(
                    target,
                    bufferHandle[0]
            )
            GL32.glBufferData(
                    target,
                    this.pixelsIntArray,
                    GL32.GL_STATIC_DRAW
            )
        } else {
            throw RuntimeException("Error generating Buffer OpenGl.")
        }
        GL32.glGenTextures(textureHandle)
        if (textureHandle[0] != 0) {
            GL32.glActiveTexture(texturaGl)
            GL32.glBindTexture(
                    target,
                    textureHandle[0]
            )
        } else {
            throw RuntimeException("Error generating Texture OpenGl.")
        }
        GL32.glTexBuffer(target, internalFormat, bufferHandle[0])
        println("END TEXTURE ")
    }

    private fun pixelArray(): TArrayPixels32 {
        return TArrayPixels32(tamTextura.x * tamTextura.y) {
            var x = (it % tamTextura.x).toDouble()
            var y = (it / tamTextura.x).toDouble()

            var corRGB = CorRGB(1.0, 1.0, 1.0)

            x -= tamTextura.x / 2
            y -= tamTextura.y / 2


            x /= fatorZoom
            y /= fatorZoom

            val x0 = 0.5
            val y0 = 0.0

            x -= x0
            y -= y0

            val iteracao = fractal.iteracoesNormalizadaI(x, y, 256, 500, 64)

            cor(iteracao).toInt()
        }
    }

    private fun desenharSprite() {

        glProgram.uniformSpritePosition.setValue(-tamTextura.x.toFloat(), +tamTextura.y.toFloat())
        glProgram.uniformSpriteSize.setValue(tamTextura.x.toFloat() * 2.0f, tamTextura.y.toFloat() * 2.0f)
        GlProgramRectangleVertex.mVertices.position(GlProgramRectangleVertex.mPositionOffset)
        glProgram.attributeVertexPosition.glVertexAttribPointer(
                GlProgramRectangleVertex.mValuesPerVertice, GL32.GL_FLOAT, false,
                GlProgramRectangleVertex.mStrideBytes, GlProgramRectangleVertex.mVertices
        )
        glProgram.attributeVertexPosition.glEnableVertexAttribArray()
        GL32.glDrawArrays(GL32.GL_TRIANGLE_STRIP, 0, 4)
    }

}
