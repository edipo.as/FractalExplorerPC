package com.playground.janelas

//todo convert GLFW to my own enums
import com.fractalExplorer.dataTypes.Vetor2d
import com.fractalExplorer.dataTypes.toVetor2d
import com.fractalExplorer.fractal.Fractal
import com.fractalExplorer.fractal.MandelbrotOtimizado
import com.fractalExplorer.openGl.Sprite
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramSpriteBufferTexture
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramSpriteTexture2d
import com.fractalExplorer.platformSpecific.windows.texturas.TexturaPaletaBuffer
import com.fractalExplorer.platformSpecific.windows.texturas.TexturaPaletaTexture2d


//todo openGl falha se a janela perde foco

class JanelaTwoPrograms(val fractal: Fractal = MandelbrotOtimizado()) : JanelaDemos() {
    private val textureBuffer = TexturaPaletaBuffer(tamTextura, pixels).apply {
        createOGLTexture()
    }
    val texture2d = TexturaPaletaTexture2d(tamTextura, pixels).apply {
        createOGLTexture()
    }

    private val glProgramBuffer = GlProgramSpriteBufferTexture().apply {
        copySetupFromTexture(textureBuffer)
    }
    private val glProgramTex2d = GlProgramSpriteTexture2d().apply {
        copySetupFromTexture(texture2d)
    }

    private val spriteBuffer = Sprite( textureBuffer).apply {
        posAbsoluteToParent = Vetor2d(-tamTextura.x.toDouble() - 700, -tamTextura.y.toDouble())
    }
    private val spriteTex2d = Sprite( texture2d).apply {
        posAbsoluteToParent = Vetor2d(-tamTextura.x.toDouble() + 700, -tamTextura.y.toDouble())
    }


    override fun aCadaQuadroGlfw() {
        glProgramBuffer.useProgram()
        glProgramBuffer.desenharSpriteQuadrado(spriteBuffer, tamTextura.toVetor2d() * 2.0)

        glProgramTex2d.useProgram()
        glProgramTex2d.desenharSpriteQuadrado(spriteTex2d, tamTextura.toVetor2d() * 2.0)

        val pidex = (mouse.x + mouse.y * tamTextura.x)
        alterarTitulo("Sprite: BufferTexture and Texture2d! Posicao: ${mouse.x} ${mouse.y} $pidex ")
    }


}

