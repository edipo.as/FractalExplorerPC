package com.playground.janelas

//todo convert GLFW to my own enums
import com.fractalExplorer.dataTypes.Vetor2d
import com.fractalExplorer.dataTypes.toVetor2d
import com.fractalExplorer.fractal.Fractal
import com.fractalExplorer.fractal.MandelbrotOtimizado
import com.fractalExplorer.openGl.Sprite
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramSpriteBufferTexture
import com.fractalExplorer.platformSpecific.windows.texturas.TexturaPaletaBuffer


//todo openGl falha se a janela perde foco

class JanelaVisualizarSpriteBufferTexture(val fractal: Fractal = MandelbrotOtimizado()) : JanelaDemos() {
    val textura = TexturaPaletaBuffer(tamTextura, pixels).apply {
        createOGLTexture()
        bind()
    }

    private val glProgram = GlProgramSpriteBufferTexture().apply {
        copySetupFromTexture(textura)
    }

    private val sprite = Sprite( textura).apply {
        posAbsoluteToParent = Vetor2d(-tamTextura.x.toDouble(), -tamTextura.y.toDouble())
    }

    override fun aCadaQuadroGlfw() {
        glProgram.useProgram()
        glProgram.desenharSpriteQuadrado(sprite, tamTextura.toVetor2d() * 2.0)

        val pidex = (mouse.x + mouse.y * tamTextura.x)
        alterarTitulo("Sprite: BufferTexture! Posicao: ${mouse.x} ${mouse.y} $pidex ")
    }

}

