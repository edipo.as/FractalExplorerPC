package com.playground.janelas

//todo convert GLFW to my own enums
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.fractal.Fractal
import com.fractalExplorer.fractal.MandelbrotOtimizado
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramRectangleVertex
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramSpriteBufferTexture
import com.fractalExplorer.platformSpecific.windows.openGl.ComputeOpenGlProgram
import com.fractalExplorer.platformSpecific.windows.openGl.printGlError
import com.fractalExplorer.platformSpecific.windows.texturas.TextureComputeFloat
import com.myGraphicLibrary.glfw.GenericGlfwWindow
import org.lwjgl.opengl.GL43
import org.lwjgl.opengl.GL32 as OGL


//todo openGl falha se a janela perde foco

@ExperimentalUnsignedTypes
class JanelaComputeShaderRaw(val fractal: Fractal = MandelbrotOtimizado()) : GenericGlfwWindow() {

    private val glComputeProgram = ComputeOpenGlProgram("compute/mandelbrotColorOutput.glsl")

    private val tamTextura = Vetor2i(1024, 1024)

    private val textureComputeOutput = TextureComputeFloat(tamTextura).apply {
        createOGLTexture()
    }

    private val glProgram = GlProgramSpriteBufferTexture("basicFragments/FragmentTexture2d.glsl").apply {
        copySetupFromTexture(textureComputeOutput)
    }

    init {
        glComputeProgram.printComputeShaderInfo()

        glComputeProgram.useProgram()
        printGlError()
        GL43.glDispatchCompute(tamTextura.x, tamTextura.y, 1)
        printGlError()
        GL43.glMemoryBarrier(GL43.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT)
        printGlError()

        val recovered = textureComputeOutput.recoverFloatArray()
        val recovered2 = textureComputeOutput.recoverIntArray()

        glProgram.useProgram()

        aCadaQuadroGlfwLambda = {
            desenharSprite()
            val pidex = (mouse.x + mouse.y * tamTextura.x)
            alterarTitulo("Posicao: ${mouse.x} ${mouse.y} , pixel index: $pidex ")
        }
    }

    private fun desenharSprite() {

        glProgram.uniformSpritePosition.setValue(-tamTextura.x.toFloat(), +tamTextura.y.toFloat())
        glProgram.uniformSpriteSize.setValue(tamTextura.x.toFloat() * 2.0f, tamTextura.y.toFloat() * 2.0f)
        GlProgramRectangleVertex.mVertices.position(GlProgramRectangleVertex.mPositionOffset)
        glProgram.attributeVertexPosition.glVertexAttribPointer(
                GlProgramRectangleVertex.mValuesPerVertice, OGL.GL_FLOAT, false,
                GlProgramRectangleVertex.mStrideBytes, GlProgramRectangleVertex.mVertices
        )
        glProgram.attributeVertexPosition.glEnableVertexAttribArray()
        OGL.glDrawArrays(OGL.GL_TRIANGLE_STRIP, 0, 4)
    }

}