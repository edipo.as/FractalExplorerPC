package com.playground.janelas

//todo convert GLFW to my own enums
import com.fractalExplorer.binds.ActionName
import com.fractalExplorer.binds.KeyboardKey
import com.fractalExplorer.dataTypes.CorRGB
import com.fractalExplorer.dataTypes.Vetor2d
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.openGl.Sprite
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramSpriteTexture2d
import com.fractalExplorer.platformSpecific.windows.texturas.TexturaPaletaTexture2d


//todo openGl falha se a janela perde foco

class JanelaVisualizarSpriteTexture2d : JanelaDemos() {
    val pixelsVerdes = IntArray(1) { CorRGB(0.0, 1.0, 0.0).toInt() }
    val texturaVerde = TexturaPaletaTexture2d(Vetor2i(1, 1), pixelsVerdes).apply {
        createOGLTexture()
        bind()
    }

    val pixelsVermelhos = IntArray(1) { CorRGB(1.0, 0.0, 0.0).toInt() }
    val texturaVermelha = TexturaPaletaTexture2d(Vetor2i(1, 1), pixelsVermelhos).apply {
        createOGLTexture()
        bind()
    }

    val pixelsAzuis = IntArray(1) { CorRGB(0.0, 0.0, 1.0).toInt() }
    val texturaAzul = TexturaPaletaTexture2d(Vetor2i(1, 1), pixelsAzuis).apply {
        createOGLTexture()
        bind()
    }

    val textura = TexturaPaletaTexture2d(tamTextura, pixels).apply {
        createOGLTexture()
        bind()
    }

    private val glProgram = GlProgramSpriteTexture2d().apply {
        copySetupFromTexture(textura)
    }

    val evenMasterer = Sprite(textura)
        .apply {
            invisible = true
            //    screenCoordinatesAbs = Vetor2d(-tamTextura.x.toDouble(), -tamTextura.y.toDouble())
            posRelative = Vetor2d(0.0, 0.0)
            sizeRelative = Vetor2d(1.0, 1.0)
        }.also {
            glProgram.sprites.add(it)
        }


    val masterSprite = Sprite(textura)
        .apply {
            //    screenCoordinatesAbs = Vetor2d(-tamTextura.x.toDouble(), -tamTextura.y.toDouble())
            posRelativeToParent = Vetor2d(0.1, 0.1)
            sizeRelativeToParent = Vetor2d(0.8, 0.8)
        }.also {
            glProgram.sprites.add(it)
        }

    val spriteVermelho = Sprite(texturaVermelha)
        .apply {
            //    screenCoordinatesAbs = Vetor2d(-tamTextura.x.toDouble(), -tamTextura.y.toDouble())
            posRelativeToParent = Vetor2d(0.1, 0.1)
            sizeRelativeToParent = Vetor2d(0.8, 0.8)
        }.also {
            glProgram.sprites.add(it)
        }

    val spriteAzul = Sprite(texturaAzul)
        .apply {
            //    screenCoordinatesAbs = Vetor2d(-tamTextura.x.toDouble(), -tamTextura.y.toDouble())
            posRelativeToParent = Vetor2d(0.0, 0.0)
            sizeRelativeToParent = Vetor2d(0.3, 1.0)
        }.also {
            glProgram.sprites.add(it)
        }

    val spriteVerde = Sprite(texturaVerde)
        .apply {
            //    screenCoordinatesAbs = Vetor2d(-tamTextura.x.toDouble(), -tamTextura.y.toDouble())
            posRelativeToParent = Vetor2d(0.0, 0.0)
            posAbsoluteToParent = Vetor2d(100.0, 100.0)
            sizeRelativeToParent = Vetor2d(1.1, 0.3)
        }.also {
            glProgram.sprites.add(it)
        }

    init {
        binds()
        evenMasterer.childs.add(masterSprite)
        masterSprite.childs.add(spriteVermelho)
        spriteVermelho.childs.add(spriteAzul)
        spriteVermelho.childs.add(spriteVerde)

        evenMasterer.fixchildrenPositions()

    }

    override fun aCadaQuadroGlfw() {




        glProgram.useProgram()
        glProgram.drawRecursive(evenMasterer)


        val pidex = (mouse.x + mouse.y * tamTextura.x)
        alterarTitulo("Sprite: Texture2d! Posicao: ${mouse.x} ${mouse.y} $pidex ")
    }

    fun binds() {
        keyBinds[KeyboardKey.KEY_R] = ActionName.CAMERA_ROTATION_INC
        keyBinds[KeyboardKey.KEY_T] = ActionName.CAMERA_ROTATION_DEC
        actionBinds[ActionName.CAMERA_ROTATION_INC] = { masterSprite.rotation += 30.0 }
        actionBinds[ActionName.CAMERA_ROTATION_DEC] = { masterSprite.rotation -= 30.0 }
    }
}

