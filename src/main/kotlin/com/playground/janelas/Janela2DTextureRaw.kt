package com.playground.janelas

//todo convert GLFW to my own enums
import com.fractalExplorer.dataTypes.ITexturaGL
import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.fractal.Fractal
import com.fractalExplorer.fractal.MandelbrotOtimizado
import com.fractalExplorer.paletas.cor
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramRectangleVertex
import com.fractalExplorer.platformSpecific.windows.glProgram.GlProgramSpriteBufferTexture
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform1i
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform2f
import com.fractalExplorer.platformSpecific.windows.openGl.Uniform2i
import com.fractalExplorer.platformSpecific.windows.openGl.printGlError
import com.myGraphicLibrary.glfw.GenericGlfwWindow
import org.lwjgl.opengl.GL32 as OGL


//todo openGl falha se a janela perde foco

@ExperimentalUnsignedTypes
class Janela2DTextureRaw(val fractal: Fractal = MandelbrotOtimizado()) : GenericGlfwWindow() {

    private val glProgram = GlProgramSpriteBufferTexture("basicFragments/FragmentTexture2d.glsl")

    //private val tamTextura = Vetor2i(dimensoesJanela.x, dimensoesJanela.y)
    private val tamTextura = Vetor2i(1024, 1024)

    val pixels = TArrayPixels32(tamTextura.x * tamTextura.y) { 0 }

    private val uniformTextura = Uniform1i("u_Textura", glProgram.programHandle)
    private val uniformWindowSize = Uniform2f("u_ViewportSize", glProgram.programHandle)
    private val uniformTextureSize = Uniform2i("u_TextureSize", glProgram.programHandle)

    private val textureHandle = IntArray(1) { 0 }
    private val texturaGl: ITexturaGL = OGL.GL_TEXTURE4
    private val idxUniformTextura: Int = 4


    private val pixelsIntArray = pixelArray()

    init {
        glProgram.useProgram()
        println("GL PROGRAM handle ${glProgram.programHandle}!")

        genGl2DTexture()
        /** TEXTURA*/

        uniformTextura.setValue(idxUniformTextura)
        uniformWindowSize.setValue(dimensoesJanela.x.toFloat(), dimensoesJanela.y.toFloat())
        uniformTextureSize.setValue(tamTextura.x, tamTextura.y)

        aCadaQuadroGlfwLambda = {

            // glProgram.glProgram.useProgram()
            // GL32.glActiveTexture(texturaGl)

            desenharSprite()

            val pidex = (mouse.x + mouse.y * tamTextura.x)
            alterarTitulo("Posicao: ${mouse.x} ${mouse.y} , pixel index: $pidex ")
        }
    }

    private fun pixelArray(): TArrayPixels32 {
        return TArrayPixels32(tamTextura.x * tamTextura.y) {
            var x = (it % tamTextura.x).toDouble()
            var y = (it / tamTextura.x).toDouble()

            x -= tamTextura.x / 2
            y -= tamTextura.y / 2

            val fatorZoom = tamTextura.x / 4.0

            x /= fatorZoom
            y /= fatorZoom

            val x0 = 0.5
            val y0 = 0.0

            x -= x0
            y -= y0

            val iteracao = fractal.iteracoesNormalizadaI(x, y, 256, 500, 64)

            cor(iteracao).toInt()
        }
    }

    private fun desenharSprite() {
        glProgram.uniformSpritePosition.setValue(-tamTextura.x.toFloat(), +tamTextura.y.toFloat())
        glProgram.uniformSpriteSize.setValue(tamTextura.x.toFloat() * 2.0f, tamTextura.y.toFloat() * 2.0f)
        GlProgramRectangleVertex.mVertices.position(GlProgramRectangleVertex.mPositionOffset)
        glProgram.attributeVertexPosition.glVertexAttribPointer(
                GlProgramRectangleVertex.mValuesPerVertice, OGL.GL_FLOAT, false,
                GlProgramRectangleVertex.mStrideBytes, GlProgramRectangleVertex.mVertices
        )
        glProgram.attributeVertexPosition.glEnableVertexAttribArray()
        OGL.glDrawArrays(OGL.GL_TRIANGLE_STRIP, 0, 4)
    }

    private fun genGl2DTexture() {
        println("START TEXTURE ")
        val target = OGL.GL_TEXTURE_2D

        /** Storage Format, Must be one of: GL_ALPHA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, GL_RGB, GL_RGBA.*/
        val internalFormat = OGL.GL_RGBA

        /** Format of Input data, Must match internalformat */
        val format = OGL.GL_RGBA

        /** type of Input Data*/
        val type = OGL.GL_UNSIGNED_BYTE

        OGL.glGenTextures(textureHandle)
        if (textureHandle[0] != 0) {
            OGL.glActiveTexture(texturaGl)
            printGlError("glActiveTexture")
            OGL.glBindTexture(
                    target,
                    textureHandle[0]
            )
            printGlError("glBindTexture")
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_WRAP_S, OGL.GL_REPEAT)
            printGlError("glTexParameteri")
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_WRAP_T, OGL.GL_REPEAT)
            printGlError("glTexParameteri")
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_MAG_FILTER, OGL.GL_NEAREST)
            printGlError("glTexParameteri")
            OGL.glTexParameteri(OGL.GL_TEXTURE_2D, OGL.GL_TEXTURE_MIN_FILTER, OGL.GL_NEAREST)
            printGlError("glTexParameteri")
            OGL.glTexImage2D(
                    target,
                    0,
                    internalFormat,
                    tamTextura.x,
                    tamTextura.y,
                    0,
                    format,
                    type,
                    pixelsIntArray
            )
            printGlError("glTexImage2D")

        } else {
            throw RuntimeException("Error generating Texture OpenGl.")
        }
        println("END TEXTURE ")
    }


}