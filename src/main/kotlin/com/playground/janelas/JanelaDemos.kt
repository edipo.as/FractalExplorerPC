package com.playground.janelas

import com.fractalExplorer.dataTypes.TArrayPixels32
import com.fractalExplorer.dataTypes.Vetor2i
import com.fractalExplorer.fractal.Fractal
import com.fractalExplorer.fractal.MandelbrotOtimizado
import com.fractalExplorer.paletas.cor
import com.myGraphicLibrary.glfw.GenericGlfwWindow

open class JanelaDemos : GenericGlfwWindow() {

    //private val tamTextura = Vetor2i(dimensoesJanela.x, dimensoesJanela.y)
    val tamTextura = Vetor2i(1024, 1024)

    val pixels = pixelArray()


    private fun pixelArray(): TArrayPixels32 {
        return TArrayPixels32(tamTextura.x * tamTextura.y) {
            var x = (it % tamTextura.x).toDouble()
            var y = (it / tamTextura.x).toDouble()

            x -= tamTextura.x / 2
            y -= tamTextura.y / 2

            val fatorZoom = tamTextura.x / 4.0

            x /= fatorZoom
            y /= fatorZoom

            val x0 = 0.5
            val y0 = 0.0

            x -= x0
            y -= y0

            val fractal: Fractal = MandelbrotOtimizado()
            val iteracao = fractal.iteracoesNormalizadaI(x, y, 256, 500, 64)

            cor(iteracao).toInt()
        }
    }

}