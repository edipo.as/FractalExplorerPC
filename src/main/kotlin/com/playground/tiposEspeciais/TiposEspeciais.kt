package com.playground.tiposEspeciais

/** declarando tipos especiais */
inline class TKelvin(val valor: Double)
inline class TCelsius(val valor: Double)
inline class PressaoPa(val valor: Double)
inline class Volume(val valor: Double)

/** definindo um conversores por extensão */
fun TCelsius.toKelvin() = TKelvin(valor + 273.15)
fun TKelvin.toCelsius() = TCelsius(valor - 273.15)

/** declarando tipos especiais */
inline class Comprimento(val value: Double)
inline class Forca(val value: Double)
inline class Momento(val value: Double)

/** sobrecarregando operador Forca*Comprimento */
operator fun Forca.times(comprimento: Comprimento) = Momento(this.value * comprimento.value)

/** propriedade comutativa **/
/** sobrecarregando operador Comprimento*Forca a partir do operador já declarado Forca*Comprimento */
operator fun Comprimento.times(forca: Forca) = forca * this