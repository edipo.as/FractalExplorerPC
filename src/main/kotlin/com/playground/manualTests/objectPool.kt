package com.playground.manualTests

import com.fractalExplorer.ObjectPool
import com.fractalExplorer.dataTypes.TArrayIteracoes
import java.lang.Thread.sleep

fun main() {
    val poolArrayIteracoes = ObjectPool(1000, 10000, 1L
    ) { TArrayIteracoes(1000) { 0 } }

    sleep(10000)

    poolArrayIteracoes.shutdown()

}