import com.playground.tiposEspeciais.*

fun main() {
    /** instanciando objetos dos tipos definidos */
    val temp1 = TKelvin(127.0)
    val temp2 = TCelsius(480.0)
    val volume1 = Volume(13.5)

    /** chamando funcao com tipos corretos**/
    val pressao1 = algumaFuncaoQueEnvolvaTempEVolume(temp1, volume1)

    /** chamando funcao com tipos corretos usando conversor de TCelsius para TKelvin **/
    val pressao2 = algumaFuncaoQueEnvolvaTempEVolume(temp2.toKelvin(), volume1)

    /** chamando funcao com tipos errados, acusa erro e não compila **/
    // val pressao3 = algumaFuncaoQueEnvolvaTempEVolume(temp2, volume1)
}

/** declarando uma funcao que só aceita tipos TKelvin e Volume */
fun algumaFuncaoQueEnvolvaTempEVolume(temp: TKelvin, volume: Volume): PressaoPa {
    val pressaoEncontrada = PressaoPa(temp.valor * volume.valor)
    println("Pressao encontrada: $pressaoEncontrada")
    return pressaoEncontrada
}
