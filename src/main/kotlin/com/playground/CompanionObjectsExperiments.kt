package com.playground

fun main() {

    val b = ClasseB.b
    val a = ClasseA.a

    println("A: $a B: $b")

    val b2 = ClasseB.b2
    val a2 = ClasseA.a2

    println("A: $a2 B: $b2")
}

class ClasseA {
    companion object {
        val a = 1
        val a2 = ClasseB.b
    }
}

class ClasseB {
    companion object {
        val b = 1
        val b2 = ClasseA.a
    }
}