#version 320 es

precision mediump float;

//Window Uniforms
uniform vec2 u_ViewportSize;// in pixels

//Renderer Uniforms
uniform vec2 u_SpritePosition;// pixels
uniform vec2 u_SpriteRelativePosition;// normalized
uniform vec2 u_SpriteSize;// pixel
uniform vec2 u_SpriteSizeRelative;// pixel
uniform vec2 u_rotacaoCosSen;

in vec2 a_RectangleVertices;// [retangulo 0,0 até 1,1]
out vec2 v_TexturePosition;

vec2 rodar(vec2 entrada){
    float cos = u_rotacaoCosSen.x;
    float sen = u_rotacaoCosSen.y;
    return vec2(entrada.x*cos+entrada.y*sen, entrada.x*-sen+entrada.y*cos);
}

void main()
{
    //todo: apenas desenha quadrados
   // a_RectangleVertices = rodar(a_RectangleVertices);

    vec2 positionAbs = ((a_RectangleVertices.xy * u_SpriteSize) + u_SpritePosition.xy)/u_ViewportSize.xy;
    vec2 positionNormalized = ((a_RectangleVertices.xy * u_SpriteSizeRelative) + u_SpriteRelativePosition.xy);

    positionNormalized.x-=0.5;
    positionNormalized.y+=0.5;
    positionNormalized*=2.0;

    float z = 1.0;// profundidade
    float w = 1.0;// Me parece que aplica uma transformação de escala *1/w

    //[-1 ate +1] com origem no centro da tela
    gl_Position = vec4(positionNormalized + positionAbs, z, w);

    v_TexturePosition = vec2(a_RectangleVertices.x, -a_RectangleVertices.y);


}