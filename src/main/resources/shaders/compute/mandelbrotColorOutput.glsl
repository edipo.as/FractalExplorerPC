#version 430

#define NAO_DIVERGIU 0

#define BRANCO  vec4(1.0, 1.0, 1.0, 1.0)
#define PRETO   vec4(0.0, 0.0, 0.0, 1.0)

layout(local_size_x = 1, local_size_y = 1) in;
layout(rgba32f, binding = 0) uniform image2D img_output;

int iteracaoMandelbrot(vec2 c){
    int maxIteracoes = 1000;
    float limiteDivergencia = 64.0;
    vec2 z = c;
    vec2 z2 = z*z;

    int i = 0;
    while (z2.x + z2.y < limiteDivergencia){
        i++;

        z.y *= z.x;
        z.y += z.y;
        z.y += c.y;
        z.x = z2.x - z2.y + c.x;
        z2 = z*z;
        if (i == maxIteracoes) return NAO_DIVERGIU;
    }
    return i;
}

vec4 getFragColor(int iteracao){
    if (iteracao == NAO_DIVERGIU) { return PRETO; }
    float densidadePaleta = 10.0;
    float theta = float(iteracao)/densidadePaleta;

    float r = 0.5+0.5*sin(theta);
    float g = 0.5+0.5*sin(theta+2.094);
    float b = 0.5+0.5*sin(theta-2.094);

    return vec4(r, g, b, 1.0);
}


void main() {
    vec2 tamTextura = vec2(1024.0, 1024.0);
    vec2 normalizedPosition = (vec2(gl_GlobalInvocationID) - tamTextura*0.5)/ tamTextura*2.0;//now ranges [-1; 1]

    vec2 positionZero = vec2(0.5, 0.0);
    normalizedPosition-=positionZero;

    float maxAbs = 1.5;
    normalizedPosition *= maxAbs;//normalizedPosition*2.0;

    int iteracao = iteracaoMandelbrot(normalizedPosition);

    vec4 color = getFragColor(iteracao);

    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);

    uvec4 ucolor = uvec4(color*pow(2.0, 32.0));

    imageStore(img_output, pixel_coords, color);
}