#version 320 es

#if __VERSION__ < 130
#define TEXTURE2D texture2D
#else
#define TEXTURE2D texture
#endif

#define VERMELHO    vec4(1.0, 0.0, 0.0, 1.0)
#define VERDE       vec4(0.0, 1.0, 0.0, 1.0)
#define AZUL        vec4(0.0, 0.0, 1.0, 1.0)
#define CIANO       vec4(0.0, 1.0, 1.0, 1.0)
#define MAGENTA     vec4(1.0, 0.0, 0.1, 1.0)
#define AMARELO     vec4(1.0, 1.0, 0.0, 1.0)

#define TRANSPARENTE 0u

precision highp float;

uniform highp usamplerBuffer u_Iteracoes;
uniform mediump samplerBuffer u_Textura;
uniform ivec2 u_TextureSize;//pixels

in vec2 v_TexturePosition;//Interpolated position for this fragment.
out vec4 fragColor;

vec4 samplePaleta(int posicao){
    int u_qtdeCoresPaleta = int(u_TextureSize.x*u_TextureSize.y);
    int pidex = posicao%(u_qtdeCoresPaleta-1);
    pidex++;
    return texelFetch(u_Textura, pidex);
}

vec4 getFragColor(){

    int x = int(floor(v_TexturePosition.x*float(u_TextureSize.x)));
    x = clamp(x, 0, u_TextureSize.x-1);
    int y = int(floor(v_TexturePosition.y*float(u_TextureSize.y)));
    y = clamp(y, 0, u_TextureSize.y-1);

    int pidex = y*int(u_TextureSize.x)+x;

    vec4 cor = samplePaleta(pidex);

    return cor;
}

void main()
{
    fragColor = getFragColor();
}
