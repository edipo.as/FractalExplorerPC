#version 320 es

#if __VERSION__ < 130
#define TEXTURE2D texture2D
#else
#define TEXTURE2D texture
#endif

#define VERMELHO    vec4(1.0, 0.0, 0.0, 1.0)
#define VERDE       vec4(0.0, 1.0, 0.0, 1.0)
#define AZUL        vec4(0.0, 0.0, 1.0, 1.0)
#define CIANO       vec4(0.0, 1.0, 1.0, 1.0)
#define MAGENTA     vec4(1.0, 0.0, 0.1, 1.0)
#define AMARELO     vec4(1.0, 1.0, 0.0, 1.0)

#define NAO_DIVERGIU 1u
#define TRANSPARENTE 0u

precision highp float;
//precision highp samplerBuffer;

uniform highp usamplerBuffer u_Iteracoes;
uniform mediump samplerBuffer u_Paleta;

uniform int u_qtdeCoresPaleta;//tamanho da paleta em pixels
uniform uint u_SamplesPorIteracao;
uniform uint u_IteracoesPorPaleta;
uniform uint u_NaoNormalizar;
uniform uint u_Imax;
uniform int u_Offset;

uniform float u_OffsetPaleta;
uniform ivec2 u_TextureSize;//pixels

in vec2 v_TexturePosition;//Interpolated position for this fragment.
in vec4 v_Color;

out vec4 fragColor;

//todo verificar os limites da paleta (problema da listra preta)
vec4 samplePaleta(int posicao){
    int pidex = posicao%(u_qtdeCoresPaleta-1);
    pidex++;
    return texelFetch(u_Paleta, pidex);
}

vec4 samplePaletaInterpolacaoLinear(float posicao){
    vec4 corAnterior = samplePaleta(int(floor(posicao)));
    vec4 proximaCor =  samplePaleta(int(ceil(posicao)));
    float pesoProximaCor = fract(posicao);
    float pesoCorAnterior = 1.0 - pesoProximaCor;
    return corAnterior*pesoCorAnterior + proximaCor*pesoProximaCor;
}

vec4 paleta(uint iteracoes){
    float velocidadeCircular = 0.1;//hardcoded
    uint samplesPorPaleta = u_SamplesPorIteracao*u_IteracoesPorPaleta;

    iteracoes -= (iteracoes%u_SamplesPorIteracao)*u_NaoNormalizar;

    iteracoes %= samplesPorPaleta;

    float posicaoIteracaoNormalizado = float(iteracoes)/float(samplesPorPaleta);//range 0.0~ (1.0-1/samplesPorPaleta)

    float posicaoNaPaleta = (posicaoIteracaoNormalizado+u_OffsetPaleta)*float(u_qtdeCoresPaleta);// sempre > 0

    vec4 cor = samplePaletaInterpolacaoLinear(float(posicaoNaPaleta));
    return cor;
}

vec4 debug(){
    float indicePaleta = v_TexturePosition.x*float(u_qtdeCoresPaleta);
    vec4 corInterpolada = samplePaletaInterpolacaoLinear(indicePaleta);
    vec4 corNaoInterpolada = samplePaleta(int(indicePaleta));

    if (uint(indicePaleta) == uint(0)) return VERMELHO;

    return corNaoInterpolada;//debug
}

vec4 getFragColor(){

    int x = int(floor(v_TexturePosition.x*float(u_TextureSize.x)));
    x = clamp(x, 0, u_TextureSize.x-1);
    int y = int(floor(v_TexturePosition.y*float(u_TextureSize.y)));
    y = clamp(y, 0, u_TextureSize.y-1);

    int pidex = y*int(u_TextureSize.x)+x;

    uint iteracoes = texelFetch(u_Iteracoes, pidex).r;

    if (iteracoes == TRANSPARENTE) return vec4(0.0, 0.0, 0.0, 1.0);

    vec4 corIteracoes = paleta(iteracoes);

    return corIteracoes;
}



void main()
{
    fragColor = getFragColor();
}
