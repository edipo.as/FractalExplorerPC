#version 320 es

precision mediump float;

//Window Uniforms
uniform vec2 u_ViewportSize;//vec2(X,Y) size in pixels

//Renderer Uniforms
uniform vec2 u_SpritePosition;//canto sup esquerdo
uniform vec2 u_SpriteSize;//pixels
uniform ivec2 u_indiceRelativoCelula;
uniform vec2 u_rotacaoCosSen;

in vec2 a_RectangleVertices;// [retangulo 0,0 até 1,1]
out vec2 v_TexturePosition;

vec2 rodar(vec2 entrada){
    float sen = u_rotacaoCosSen.y;
    float cos = u_rotacaoCosSen.x;
    return vec2(entrada.x*cos+entrada.y*sen, entrada.x*-sen+entrada.y*cos);
}

void main()
{
    ivec2 posicaoRelativaCelula = ivec2((u_indiceRelativoCelula.x), (-u_indiceRelativoCelula.y));

    vec2 posicaoCantoSprite =  u_SpritePosition + vec2(posicaoRelativaCelula)*u_SpriteSize;

    vec2 posicao_tela = (a_RectangleVertices.xy * u_SpriteSize + posicaoCantoSprite);

    float z = 1.0;// profundidade
    float w = 1.0;// Me parece que aplica uma transformação de escala *1/w

    //[-1 ate +1] com origem no centro da tela
    gl_Position = vec4(rodar(posicao_tela)/u_ViewportSize.xy, z, w);

    v_TexturePosition = vec2(a_RectangleVertices.x, -a_RectangleVertices.y);
}