#version 320 es

#if __VERSION__ < 130
#define TEXTURE2D texture2D
#else
#define TEXTURE2D texture
#endif

#define VERMELHO    vec4(1.0, 0.0, 0.0, 1.0)
#define VERDE       vec4(0.0, 1.0, 0.0, 1.0)
#define AZUL        vec4(0.0, 0.0, 1.0, 1.0)
#define CIANO       vec4(0.0, 1.0, 1.0, 1.0)
#define MAGENTA     vec4(1.0, 0.0, 0.1, 1.0)
#define AMARELO     vec4(1.0, 1.0, 0.0, 1.0)

precision mediump float;

uniform mediump sampler2D u_Textura;// The input texture.

in vec2 v_TexturePosition;// Interpolated position for this fragment.
out vec4 fragColor;

void main()
{
    fragColor = (texture(u_Textura, v_TexturePosition.xy))*1.0;
  //  if (fragColor.a == 0.0)fragColor.a  = 1.0;
}